package sqli.lang.syntactical;

import sqli.lang.absyn.*;
import sqli.lang.lexical.Lexer;

parser code {:
    private Lexer lexer;

    public Parser(Lexer lexer) { this.lexer = lexer; }

    public void syntax_error(java_cup.runtime.Symbol token) throws SyntaxErrorException {
        throw new SyntaxErrorException("Syntax error at token " + sym.terminalNames[token.sym] + " @" + token.left + ":" + token.right);
    }
:};

scan with {: return lexer.next_token(); :};

terminal Integer INT_LITERAL;
terminal Float FLOAT_LITERAL;
terminal Boolean BOOL_LITERAL;
terminal String STR_LITERAL, ID;
terminal PLUS, MINUS, TIMES, DIVIDE, AND, OR, GEQ, LEQ, GT, LT, EQ, NEQ, NOT, CONCAT, SUBSTR, LEN, ASSIGN, STR, INT,
         BOOL, UNTRUSTED_STR, UNTRUSTED_INT, UNTRUSTED_BOOL, SEMICOLON, COMMA, RPAR, LPAR, WHILE, IF, THEN, ELSE,
         EXECUTE, ELSEIF, LBRACE, RBRACE, SKIP, END, FLOAT, UNTRUSTED_FLOAT, TOINT, TOSTR, TOBOOL, TOFLOAT, FUNCTION,
         RETURN, PROGSEC, FUNSEC, VARSEC, COMSEC, PROTSEC;

non terminal ProgDefinition prog;
non terminal FunctionDefinition funsdefs;
non terminal FunctionDeclaration fundecl;
non terminal FunctionPrototype funsprots;
non terminal ParamDeclaration paramsdecls;
non terminal VariableDeclaration vardecl;
non terminal VariableDefinition varsdefs, funvarsdefs;
non terminal TypeDeclaration type;
non terminal Expression exp, expplus;
non terminal ActualParameter arguments;
non terminal Command comseq, com;
non terminal IfThenElse elseif;
non terminal VariableName var;
non terminal FunctionName fun;
non terminal ProgramName prg;

precedence nonassoc THEN;
precedence nonassoc ELSEIF;
precedence nonassoc ELSE;

precedence left NOT;
precedence left AND, OR;
precedence nonassoc EQ, NEQ, LT, LEQ, GT, GEQ;
precedence left PLUS, MINUS;
precedence left TIMES, DIVIDE;

start with prog;

prog        ::= PROGSEC prg:id PROTSEC funsprots:prots FUNSEC funsdefs:funs VARSEC varsdefs:vars COMSEC comseq:com    {: RESULT = new ProgDefinition(idleft, idright, id, prots, funs, vars, com); :} ;

funsprots   ::= fundecl:sig SEMICOLON funsprots:next                                                                  {: RESULT = new FunctionPrototype(sigleft, sigright, sig, next); :}
              |                                                                                                       {: RESULT = null; :} ;

funsdefs    ::= fundecl:sig LBRACE funvarsdefs:vars comseq:body RBRACE SEMICOLON funsdefs:next                        {: RESULT = new FunctionDefinition(sigleft, sigright, sig, vars, body, next); :}
              |                                                                                                       {: RESULT = null; :} ;

fundecl     ::= type:type FUNCTION fun:id LPAR paramsdecls:params RPAR                                                {: RESULT = new FunctionDeclaration(typeleft, typeright, type, id, params); :} ;

paramsdecls ::= vardecl:decl COMMA paramsdecls:next                                                                   {: RESULT = new ParamDeclaration(declleft, declright, decl, next); :}
              | vardecl:decl                                                                                          {: RESULT = new ParamDeclaration(declleft, declright, decl, null); :}
              |                                                                                                       {: RESULT = null; :} ;

vardecl     ::= type:type var:id                                                                                      {: RESULT = new VariableDeclaration(typeleft, typeright, type, id); :} ;

varsdefs    ::= vardecl:decl ASSIGN expplus:exp SEMICOLON varsdefs:next                                               {: RESULT = new VariableDefinition(declleft, declright, decl, exp, next); :}
              |                                                                                                       {: RESULT = null; :} ;

funvarsdefs ::= vardecl:decl ASSIGN exp:exp SEMICOLON funvarsdefs:next                                                {: RESULT = new VariableDefinition(declleft, declright, decl, exp, next); :}
              |                                                                                                       {: RESULT = null; :} ;

type        ::= STR:str                                                                                               {: RESULT = new StrTypeDeclaration(strleft, strright); :}
              | INT:_int                                                                                              {: RESULT = new IntTypeDeclaration(_intleft, _intright); :}
              | BOOL:bool                                                                                             {: RESULT = new BoolTypeDeclaration(boolleft, boolright); :}
              | FLOAT:_float                                                                                          {: RESULT = new FloatTypeDeclaration(_floatleft, _floatright); :} ;

expplus     ::= exp:exp                                                                                               {: RESULT = exp; :}
              | UNTRUSTED_STR:us LPAR RPAR                                                                            {: RESULT = new UntrustedStr(usleft, usright); :}
              | UNTRUSTED_INT:ui LPAR RPAR                                                                            {: RESULT = new UntrustedInt(uileft, uiright); :}
              | UNTRUSTED_BOOL:ub LPAR RPAR                                                                           {: RESULT = new UntrustedBool(ubleft, ubright); :}
              | UNTRUSTED_FLOAT:uf LPAR RPAR                                                                          {: RESULT = new UntrustedFloat(ufleft, ufright); :} ;

exp         ::= STR_LITERAL:s                                                                                         {: RESULT = new StringLiteral(sleft, sright, s); :}
              | INT_LITERAL:i                                                                                         {: RESULT = new IntegerLiteral(ileft, iright, i.intValue()); :}
              | BOOL_LITERAL:b                                                                                        {: RESULT = new BooleanLiteral(bleft, bright, b.booleanValue()); :}
              | FLOAT_LITERAL:f                                                                                       {: RESULT = new FloatLiteral(fleft, fright, f.floatValue()); :}
              | CONCAT:concat LPAR exp:left COMMA exp:right RPAR                                                      {: RESULT = new Concatenation(concatleft, concatright, left, right); :}
              | SUBSTR:substr LPAR exp:exp COMMA exp:i1 COMMA exp:i2 RPAR                                             {: RESULT = new Substring(substrleft, substrright, exp, i1, i2); :}
              | LEN:len LPAR exp:exp RPAR                                                                             {: RESULT = new Length(lenleft, lenright, exp); :}
              | exp:left PLUS:plus exp:right                                                                          {: RESULT = new Addition(plusleft, plusright, left, right); :}
              | exp:left MINUS:minus exp:right                                                                        {: RESULT = new Subtraction(minusleft, minusright, left, right); :}
              | exp:left TIMES:times exp:right                                                                        {: RESULT = new Multiplication(timesleft, timesright, left, right); :}
              | exp:left DIVIDE:divide exp:right                                                                      {: RESULT = new Division(divideleft, divideright, left, right); :}
              | MINUS:minus exp:exp                                                                                   {: RESULT = new Minus(minusleft, minusright, exp); :}
              | exp:left AND:and exp:right                                                                            {: RESULT = new And(andleft, andright, left, right); :}
              | exp:left OR:or exp:right                                                                              {: RESULT = new Or(orleft, orright, left, right); :}
              | NOT:not exp:exp                                                                                       {: RESULT = new Not(notleft, notright, exp); :}
              | exp:left EQ:eq exp:right                                                                              {: RESULT = new Equal(eqleft, eqright, left, right); :}
              | exp:left NEQ:neq exp:right                                                                            {: RESULT = new NotEqual(neqleft, neqright, left, right); :}
              | exp:left GEQ:geq exp:right                                                                            {: RESULT = new GreaterThanOrEqual(geqleft, geqright, left, right); :}
              | exp:left LEQ:leq exp:right                                                                            {: RESULT = new LessThanOrEqual(leqleft, leqright, left, right); :}
              | exp:left GT:gt exp:right                                                                              {: RESULT = new GreaterThan(gtleft, gtright, left, right); :}
              | exp:left LT:lt exp:right                                                                              {: RESULT = new LessThan(ltleft, ltright, left, right); :}
              | TOSTR:ts LPAR exp:exp RPAR                                                                            {: RESULT = new ToStr(tsleft, tsright, exp); :}
              | TOINT:ti LPAR exp:exp RPAR                                                                            {: RESULT = new ToInt(tileft, tiright, exp); :}
              | TOBOOL:tb LPAR exp:exp RPAR                                                                           {: RESULT = new ToBool(tbleft, tbright, exp); :}
              | TOFLOAT:tf LPAR exp:exp RPAR                                                                          {: RESULT = new ToFloat(tfleft, tfright, exp); :}
              | var:id                                                                                                {: RESULT = new Deref(idleft, idright, id); :}
              | LPAR exp:exp RPAR                                                                                     {: RESULT = exp; :}
              | fun:id LPAR arguments:args RPAR                                                                       {: RESULT = new Call(idleft, idright, id, args); :} ;

arguments   ::= exp:exp COMMA arguments:next                                                                          {: RESULT = new ActualParameter(expleft, expright, exp, next); :}
              | exp:exp                                                                                               {: RESULT = new ActualParameter(expleft, expright, exp, null); :}
              |                                                                                                       {: RESULT = null; :} ;

comseq      ::= com:first comseq:second                                                                               {: RESULT = new CommandSeq(firstleft, firstright, first, second); :}
              | com:com                                                                                               {: RESULT = com; :} ;

com         ::= var:id ASSIGN:assign exp:exp SEMICOLON                                                                {: RESULT = new Assignment(assignleft, assignright, id, exp); :}
              | WHILE:_while LPAR exp:cond RPAR LBRACE comseq:body RBRACE                                             {: RESULT = new While(_whileleft, _whileright, cond, body); :}
              | EXECUTE:exec LPAR exp:deref RPAR SEMICOLON                                                            {: RESULT = new Execute(execleft, execright, deref); :}
              | IF:_if LPAR exp:cond RPAR THEN LBRACE comseq:then RBRACE elseif:_else                                 {: RESULT = new IfThenElse(_ifleft, _ifright, cond, then, _else); :}
              | IF:_if LPAR exp:cond RPAR THEN LBRACE comseq:then RBRACE ELSE LBRACE comseq:_else RBRACE              {: RESULT = new IfThenElse(_ifleft, _ifright, cond, then, _else); :}
              | IF:_if LPAR exp:cond RPAR THEN LBRACE comseq:then RBRACE                                              {: RESULT = new IfThenElse(_ifleft, _ifright, cond, then); :}
              | SKIP:skip SEMICOLON                                                                                   {: RESULT = new Skip(skipleft, skipright); :}
              | END:end SEMICOLON                                                                                     {: RESULT = new End(endleft, endright); :}
              | RETURN:ret SEMICOLON                                                                                  {: RESULT = new Return(retleft, retright, null); :}
              | RETURN:ret exp:exp SEMICOLON                                                                          {: RESULT = new Return(retleft, retright, exp); :} ;

elseif      ::= ELSEIF:elif LPAR exp:cond RPAR THEN LBRACE comseq:then RBRACE elseif:_else                            {: RESULT = new IfThenElse(elifleft, elifright, cond, then, _else); :}
              | ELSEIF:elif LPAR exp:cond RPAR THEN LBRACE comseq:then RBRACE ELSE LBRACE com:_else RBRACE            {: RESULT = new IfThenElse(elifleft, elifright, cond, then, _else); :}
              | ELSEIF:elif LPAR exp:cond RPAR THEN LBRACE comseq:then RBRACE                                         {: RESULT = new IfThenElse(elifleft, elifright, cond, then); :} ;

var         ::= ID:id                                                                                                 {: RESULT = new VariableName(idleft, idright, id); :} ;

fun         ::= ID:id                                                                                                 {: RESULT = new FunctionName(idleft, idright, id); :} ;

prg         ::= ID:id                                                                                                 {: RESULT = new ProgramName(idleft, idright, id); :} ;