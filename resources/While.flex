package sqli.lang.lexical;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

import java_cup.runtime.Symbol;

import sqli.lang.syntactical.sym;

import static controller.ErrorsHandler.err;

%%

%public
%class Lexer
%unicode
%cup
%line
%column
%yylexthrow LexicalAnalyzerException

%eofval{
    if (zzLexicalState != YYINITIAL)
        throw new FinalStateException("Lexer did not terminate in YYINITIAL state");

    return symbol(sym.EOF, null);
%eofval}

%{
    public static Lexer mkLexer(String fileName) {
        Lexer lexer = null;

        try {
            File file = new File(fileName);
            Reader reader = new FileReader(file);
            lexer = new Lexer(reader);
        } catch (FileNotFoundException exp) {
            err("Cannot find " + fileName);
        }

        return lexer;
    }

    StringBuffer string = new StringBuffer();

    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

LineTerminator = \r|\n|\r\n
WhiteSpace = {LineTerminator} | [ \t\f]
Identifier = [a-zA-Z][a-zA-Z0-9]*
DecIntegerLiteral = 0 | [1-9][0-9]*
DecFloatLiteral = {DecIntegerLiteral}"."[0-9]+

%state STRING

%%

/* Sections. */
<YYINITIAL> ".PROGRAM_NAME"        { return symbol(sym.PROGSEC, null); }
<YYINITIAL> ".FUNCTIONS"           { return symbol(sym.FUNSEC, null); }
<YYINITIAL> ".VARIABLES"           { return symbol(sym.VARSEC, null); }
<YYINITIAL> ".COMMANDS"            { return symbol(sym.COMSEC, null); }
<YYINITIAL> ".PROTOTYPES"          { return symbol(sym.PROTSEC, null); }

/* Keywords. */
<YYINITIAL> "int"                  { return symbol(sym.INT, null); }
<YYINITIAL> "str"                  { return symbol(sym.STR, null); }
<YYINITIAL> "bool"                 { return symbol(sym.BOOL, null); }
<YYINITIAL> "float"                { return symbol(sym.FLOAT, null); }
<YYINITIAL> "while"                { return symbol(sym.WHILE, null); }
<YYINITIAL> "if"                   { return symbol(sym.IF, null); }
<YYINITIAL> "then"                 { return symbol(sym.THEN, null); }
<YYINITIAL> "else"                 { return symbol(sym.ELSE, null); }
<YYINITIAL> "execute"              { return symbol(sym.EXECUTE, null); }
<YYINITIAL> "else if"              { return symbol(sym.ELSEIF, null); }
<YYINITIAL> "skip"                 { return symbol(sym.SKIP, null); }
<YYINITIAL> "end"                  { return symbol(sym.END, null); }
<YYINITIAL> "function"             { return symbol(sym.FUNCTION, null); }
<YYINITIAL> "return"               { return symbol(sym.RETURN, null); }

/* Operators. */
<YYINITIAL> ":="                   { return symbol(sym.ASSIGN, null); }
<YYINITIAL> "+"                    { return symbol(sym.PLUS, null); }
<YYINITIAL> "-"                    { return symbol(sym.MINUS, null); }
<YYINITIAL> "*"                    { return symbol(sym.TIMES, null); }
<YYINITIAL> "/"                    { return symbol(sym.DIVIDE, null); }
<YYINITIAL> "!="                   { return symbol(sym.NEQ, null); }
<YYINITIAL> "=="                   { return symbol(sym.EQ, null); }
<YYINITIAL> "&&"                   { return symbol(sym.AND, null); }
<YYINITIAL> "!"                    { return symbol(sym.NOT, null); }
<YYINITIAL> "||"                   { return symbol(sym.OR, null); }
<YYINITIAL> ">"                    { return symbol(sym.GT, null); }
<YYINITIAL> "<"                    { return symbol(sym.LT, null); }
<YYINITIAL> ">="                   { return symbol(sym.GEQ, null); }
<YYINITIAL> "<="                   { return symbol(sym.LEQ, null); }
<YYINITIAL> "len"                  { return symbol(sym.LEN, null); }
<YYINITIAL> "concat"               { return symbol(sym.CONCAT, null); }
<YYINITIAL> "substr"               { return symbol(sym.SUBSTR, null); }
<YYINITIAL> "untrusted_str"        { return symbol(sym.UNTRUSTED_STR, null); }
<YYINITIAL> "untrusted_int"        { return symbol(sym.UNTRUSTED_INT, null); }
<YYINITIAL> "untrusted_bool"       { return symbol(sym.UNTRUSTED_BOOL, null); }
<YYINITIAL> "untrusted_float"      { return symbol(sym.UNTRUSTED_FLOAT, null); }
<YYINITIAL> "toint"                { return symbol(sym.TOINT, null); }
<YYINITIAL> "tostr"                { return symbol(sym.TOSTR, null); }
<YYINITIAL> "tobool"               { return symbol(sym.TOBOOL, null); }
<YYINITIAL> "tofloat"              { return symbol(sym.TOFLOAT, null); }

/* Separators and parenthesis. */
<YYINITIAL> ";"                    { return symbol(sym.SEMICOLON, null); }
<YYINITIAL> "("                    { return symbol(sym.LPAR, null); }
<YYINITIAL> ")"                    { return symbol(sym.RPAR, null); }
<YYINITIAL> "{"                    { return symbol(sym.LBRACE, null); }
<YYINITIAL> "}"                    { return symbol(sym.RBRACE, null); }
<YYINITIAL> ","                    { return symbol(sym.COMMA, null); }

/* Literals. */
<YYINITIAL> {DecIntegerLiteral}    { return symbol(sym.INT_LITERAL, new Integer(yytext())); }
<YYINITIAL> {DecFloatLiteral}      { return symbol(sym.FLOAT_LITERAL, new Float(yytext())); }

<YYINITIAL> "false"                { return symbol(sym.BOOL_LITERAL, Boolean.FALSE); }
<YYINITIAL> "true"                 { return symbol(sym.BOOL_LITERAL, Boolean.TRUE); }

<YYINITIAL> \"                     { string.setLength(0); yybegin(STRING); }
<STRING> \"                        { yybegin(YYINITIAL); return symbol(sym.STR_LITERAL, string.toString()); }
<STRING> [^\n\r\"\\]+              { string.append(yytext()); }
<STRING> \\t                       { string.append('\t'); }
<STRING> \\n                       { string.append('\n'); }
<STRING> \\r                       { string.append('\r'); }
<STRING> \\\"                      { string.append('\"'); }
<STRING> \\                        { string.append('\\'); }

/* Identifiers. */
<YYINITIAL> {Identifier}           { return symbol(sym.ID, yytext()); }

/* Whitespace. */
<YYINITIAL> {WhiteSpace}           { /* ignore */ }

/* Error fallback. */
[^]                                { throw new UnmatchedInputException("Illegal character <" + yytext() + "> @" + yyline + ":" + yycolumn); }
