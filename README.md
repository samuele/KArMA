# KArMA

KArMA (Knowledge-Aided Monitoring Approach) is a «proof of concept» tool to
detect and fix SQL injection vulnerabilities in web applications written in a
simple but Turing complete toy language.

The prototype shall be understood as an implementation of a scientific paper
rather than a commercial/professional tool.

## Brief description

KArMA is both a static and dynamic analysis tool. The static phase aims to
detect vulnerable untrusted inputs that may potentially interfere with a query
execution. More precisely, for each path on which an untrusted input interferes
with a query, we ask the programmer to fix the intended structure of the input
by a contract (a regular expression or a context-free grammar). Once we
collected all these contracts, the application runs under the supervision of a
monitor (dynamic phase) which ensures that all contracts will be satisfied
at runtime.

## How to use the tool

### Prerequisites

The tool is entirely written in Java 8 on Ubuntu 16.04 and tested on Linux,
Windows and Mac OS. The following tool/libraries must be installed to work
properly:

* [Java 8](https://www.java.com/en/)
* [Graphviz](http://www.graphviz.org)
* [JavaFX](http://docs.oracle.com/javase/8/javafx/get-started-tutorial/jfx-overview.htm)

Make sure that following commands work in console:
    
    user@host:~$ java -version
    openjdk version "1.8.0_121"
    OpenJDK Runtime Environment (build 1.8.0_121-8u121-b13-0ubuntu1.16.04.2-b13)
    OpenJDK 64-Bit Server VM (build 25.121-b13, mixed mode)
    
    user@host:~$ dot -?
    Usage: dot [-Vv?] [-(GNE)name=val] [-(KTlso)<val>] <dot files>
    ...
    
To install JavaFX on Ubuntu (if missing), simply run
   
    user@host:~$ sudo apt-get install openjfx

### Cloning and first run

#### Cloning

The project can be easily cloned via `git`
(`git clone https://gitlab.com/samuele/KArMA.git`) or it can be downloaded from
the [project main page](https://gitlab.com/samuele/KArMA).

#### First run

    user@host:~/KArMA$ cd bin
    user@host:~/KArMA/bin$ java -jar KArMA.jar
    
## Step-by-step guide

### Program loading

In the initial phase the program to analyze shall be loaded in the tool. Some
illustrative examples (including the one in the paper) are saved in
`KArMA/progs`.

### Static phases

#### Lexical analysis

Convert the source code into a sequence of tokens and save the output (a `*.lex`
file) in `KArMA/bin/out` folder.

#### Syntactical analysis

Parse the source code and output the abstract syntax tree (`*_absyn.dot` file).

#### Semantical analysis

Verify the type safety of the program and output the typed abstract syntax tree
(`*_types.dot`).

#### Control Flow Graph

Build the CFG of the program and output:
* `*_cfg.dot` - The CFG of the program;
* `*_doms.dot` - The CFG of the program enriched with dominators edges;
* `*_frontiers.dot` - The CFG of the program enriched with the dominance frontier
of each basic block;
* `*_domtree.dot` - The dominator tree of the program.

#### Static Single Assignment form

Build the SSA form of the program and output the resulting CFG (`*_cfgssa.dot`
file).

#### Taint analysis

Run a reaching definition based static taint analysis to detect vulnerable
untrusted inputs that intefere with a query execution and output the resulting
tree (`*_taint.dot` file).

#### Contracts request

For each vulnerable path (leading from an untrusted input to a query execution)
ask the programmer a contract (a [Java regular expression](http://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html))
and output the annotated paths (`*_contracts.dot` file).

### Dynamic phase

#### Monitoring

Run the program and check if the given contracts are satisfied: if one of them is
not satisfied an exception will be thrown.

## Toy language grammar

The (`cup`) grammar of the toy language is saved in `resources/While.cup`.
    





