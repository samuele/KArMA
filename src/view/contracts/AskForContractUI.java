package view.contracts;

import com.guigarage.flatterfx.FlatterFX;
import controller.ProcessHandler;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import sqli.analysis.contracts.Path;
import view.root.App;

import java.io.IOException;

public class AskForContractUI extends Stage {

    private static final String fxml = "askforcontract.fxml";
    private static final String title = "KArMA | Contract request";
    private static final double width = 800;
    private static final double height = 600;

    private final Path path;

    public AskForContractUI(Path path) {
        this.path = path;

        ProcessHandler pHandler = ProcessHandler.getInstance();
        pHandler.setAskForContractStage(this);

        initAskForContractLayout();
    }

    Path getPath() {
        return path;
    }

    private void initAskForContractLayout() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(fxml));
        } catch (IOException e) {
            System.err.println("Cannot load " + fxml + " style: " + e.getMessage());
            e.printStackTrace();
            Platform.exit();
        }

        setTitle(title + " (" + path.getIdentifier() + "/" + (Path.getCounter() - 1) + ")");

        this.getIcons().add(
                new Image(
                        App.class.getResourceAsStream("/icon.png")));

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        setX((screenBounds.getWidth() - width) / 2);
        setY((screenBounds.getHeight() - height) / 2);

        //noinspection ConstantConditions
        setScene(new Scene(root, width, height));

        FlatterFX.style();
    }
}
