package view.contracts;

import controller.ProcessHandler;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.WindowEvent;
import sqli.analysis.contracts.Contract;
import sqli.analysis.contracts.Path;
import sqli.analysis.contracts.ValidationContract;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AskForContractController implements Initializable {

    @FXML
    private Button command;

    private ProcessHandler pHandler;
    private Path path;
    private boolean nextBehavior;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pHandler = ProcessHandler.getInstance();
        path = pHandler.getAskForContractStage().getPath();

        AskForContractUI stage = pHandler.getAskForContractStage();
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                if (path.getContract() != null)
                    return;

                pHandler.alert("You must give valid contract before closing this window", "Closing error",
                        "Invalid contracts", Alert.AlertType.ERROR);
                event.consume();
            }
        });
    }

    @FXML
    protected void onActionAskForContractButton() {
        if (nextBehavior) {
            pHandler.closeAskForContractStage();
        } else {
            path.setContract(new Contract(path, ValidationContract.build()));
            nextBehavior = true;
            command.setText("Close");
        }
    }

    @FXML
    protected void onActionShowPathButton() {
        try {
            String file = "output/" + pHandler.getAbsyn().getName().getId() + "_path" + path.getIdentifier();
            String dotFile = file + ".dot";
            String pdfFile = file + ".pdf";

            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec("dot -Tpdf " + dotFile + " -o " + pdfFile);
            pr.waitFor();

            new Thread(() -> {
                try {
                    File pathPDF = new File(pdfFile);
                    Desktop d = Desktop.getDesktop();
                    d.open(pathPDF);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (InterruptedException e) {
            pHandler.asyncErr("Unexpected error: " + e.getMessage(), "InterruptedException error");
        }catch (IOException e) {
            pHandler.asyncErr("Unexpected error during the .dot file compilation. Make sure this command works in console:\n$> dot -?", "Generation error");
        }
    }
}
