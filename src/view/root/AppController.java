package view.root;

import controller.ProcessHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class AppController implements Initializable {

    private ProcessHandler pHandler;

    @FXML
    private TextField pathToSource;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pHandler = ProcessHandler.getInstance();
    }

    @FXML
    protected void onActionLoadButton() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select the .while file to analyze");
        File choice = fileChooser.showOpenDialog(new Stage());

        if (choice != null)
            pathToSource.setText(choice.getAbsolutePath());
    }

    @FXML
    protected void onActionAnalyzeButton() {
        if (!pHandler.init(pathToSource.getText()))
            return;

        pHandler.spawnLexicalStage();
    }
}
