package view.root;

import com.guigarage.flatterfx.FlatterFX;
import controller.ErrorsHandler;
import controller.ProcessHandler;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class App extends Application {

    public static Application app;

    private static final String fxml = "root.fxml";
    private static final String title = "KArMA";
    private static final double width = 800;
    private static final double height = 600;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        initRootLayout(primaryStage);

        ProcessHandler pHandler = ProcessHandler.getInstance();
        pHandler.setPrimaryStage(primaryStage);

        ErrorsHandler.init(pHandler);

        new File("output").mkdirs();
        app = this;
    }

    private void initRootLayout(Stage primaryStage) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(fxml));
        } catch (IOException e) {
            System.err.println("Cannot load " + fxml + " style: " + e.getMessage());
            e.printStackTrace();
            Platform.exit();
        }

        primaryStage.setTitle(title);


        primaryStage.getIcons().add(
                new Image(
                        App.class.getResourceAsStream("/icon.png")));

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth() - width) / 2);
        primaryStage.setY((screenBounds.getHeight() - height) / 2);

        //noinspection ConstantConditions
        primaryStage.setScene(new Scene(root, width, height));
        primaryStage.show();

        FlatterFX.style();
    }
}
