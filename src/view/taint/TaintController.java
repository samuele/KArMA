package view.taint;

import controller.ProcessHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TaintController implements Initializable {

    @FXML
    private Button command;
    @FXML
    private ProgressIndicator progress;
    @FXML
    private HBox content;

    private ProcessHandler pHandler;
    private boolean closeBehavior;
    private boolean nextBehavior;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pHandler = ProcessHandler.getInstance();
    }

    @FXML
    protected void onActionTaintButton() {
        if (closeBehavior) {
            pHandler.closeTaintStage();
            pHandler.end();
        }
        else if (nextBehavior)
            pHandler.spawnContractsStage();
        else if (pHandler.startTaintAnalysis()) {
            nextBehavior = true;
            command.setText("Next > Contracts request");
            progress.setProgress(0.75);
            loadContent();
        } else {
            closeBehavior = true;
            command.setText("Close");
        }
    }

    @FXML
    protected void onActionShowTreesButton() {
        try {
            String file = "output/" + pHandler.getAbsyn().getName().getId() + "_taint";
            String dotFile = file + ".dot";
            String pdfFile = file + ".pdf";

            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec("dot -Tpdf " + dotFile + " -o " + pdfFile);
            pr.waitFor();

            new Thread(() -> {
                try {
                    File taintPDF = new File(pdfFile);
                    Desktop d = Desktop.getDesktop();
                    d.open(taintPDF);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (InterruptedException e) {
            pHandler.asyncErr("Unexpected error: " + e.getMessage(), "InterruptedException error");
        }catch (IOException e) {
            pHandler.asyncErr("Unexpected error during the .dot file compilation. Make sure this command works in console:\n$> dot -?", "Generation error");
        }
    }

    private void loadContent() {
        content.setManaged(true);
        content.setVisible(true);
    }
}
