package view.lexical;

import controller.ProcessHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static utils.Utils.toLex;

public class LexicalController implements Initializable {

    @FXML
    private Button command;
    @FXML
    private ProgressIndicator progress;
    @FXML
    private TextArea result;

    private ProcessHandler pHandler;
    private boolean closeBehavior;
    private boolean nextBehavior;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pHandler = ProcessHandler.getInstance();
    }

    @FXML
    protected void onActionLexicalButton() {
        if (closeBehavior) {
            pHandler.closeLexicalStage();
            pHandler.end();
        }
        else if (nextBehavior)
            pHandler.spawnSyntacticalStage();

        else if (pHandler.startLexicalAnalysis()) {
            nextBehavior = true;
            command.setText("Next > Syntactical analysis");
            progress.setProgress(0.125);
            loadContent(toLex(pHandler.getSource()));
        } else {
            closeBehavior = true;
            command.setText("Close");
            loadContent(toLex(pHandler.getSource()));
        }
    }

    private void loadContent(String lex) {
        try (BufferedReader br = new BufferedReader(new FileReader(lex))) {
            String line;
            while ((line = br.readLine()) != null)
                result.appendText(line + "\n");
        } catch (IOException e) {
            pHandler.asyncErr("Cannot display file " + lex + " content: " + e.getMessage(), "I/O error");
        }

        result.setManaged(true);
        result.setVisible(true);
    }
}
