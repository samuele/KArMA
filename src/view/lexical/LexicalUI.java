package view.lexical;

import com.guigarage.flatterfx.FlatterFX;
import controller.ProcessHandler;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import view.root.App;

import java.io.IOException;

public class LexicalUI extends Stage {

    private static final String fxml = "lexical.fxml";
    private static final String title = "KArMA | Lexical analysis";
    private static final double width = 800;
    private static final double height = 600;

    public LexicalUI() {
        initLexicalLayout();

        ProcessHandler pHandler = ProcessHandler.getInstance();
        pHandler.setLexicalStage(this);

        setOnCloseRequest(windowEvent -> Platform.exit());
    }

    private void initLexicalLayout() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(fxml));
        } catch (IOException e) {
            System.err.println("Cannot load " + fxml + " style: " + e.getMessage());
            e.printStackTrace();
            Platform.exit();
        }

        setTitle(title);

        this.getIcons().add(
                new Image(
                        App.class.getResourceAsStream("/icon.png")));

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        setX((screenBounds.getWidth() - width) / 2);
        setY((screenBounds.getHeight() - height) / 2);

        //noinspection ConstantConditions
        setScene(new Scene(root, width, height));

        FlatterFX.style();
    }
}
