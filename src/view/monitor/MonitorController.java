package view.monitor;

import controller.ProcessHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static utils.Utils.toMon;

public class MonitorController implements Initializable {

    @FXML
    private Button command;
    @FXML
    private ProgressIndicator progress;
    @FXML
    private TextArea result;

    private ProcessHandler pHandler;
    private boolean closeBehavior;
    private boolean nextBehavior;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pHandler = ProcessHandler.getInstance();
    }

    @FXML
    protected void onActionMonitoringButton() {
        if (closeBehavior)
            pHandler.closeMonitorStage();
        else if (nextBehavior) {
            pHandler.closeMonitorStage();
            pHandler.end();
        }
        else if (pHandler.startMonitoring()) {
            nextBehavior = true;
            command.setText("Close");
            progress.setProgress(1);
            loadContent(toMon(pHandler.getSource()));
        } else {
            closeBehavior = true;
            command.setText("Close");
            loadContent(toMon(pHandler.getSource()));
        }
    }

    private void loadContent(String mon) {
        try (BufferedReader br = new BufferedReader(new FileReader(mon))) {
            String line;
            while ((line = br.readLine()) != null)
                result.appendText(line + "\n");
        } catch (IOException e) {
            pHandler.asyncErr("Cannot display file " + mon + " content: " + e.getMessage(), "I/O error");
        }

        result.setManaged(true);
        result.setVisible(true);
    }
}
