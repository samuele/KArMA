package view.cfg;

import controller.ProcessHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CFGController implements Initializable {

    @FXML
    private Button command;
    @FXML
    private ProgressIndicator progress;
    @FXML
    private HBox content;

    private ProcessHandler pHandler;
    private boolean closeBehavior;
    private boolean nextBehavior;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pHandler = ProcessHandler.getInstance();
    }

    @FXML
    protected void onActionCFGButton() {
        if (closeBehavior) {
            pHandler.closeCFGStage();
            pHandler.end();
        }
        else if (nextBehavior)
            pHandler.spawnSSAStage();
        else if (pHandler.startCFGConstruction()) {
            nextBehavior = true;
            command.setText("Next > Static Single Assignment form construction");
            progress.setProgress(0.5);
            loadContent();
        } else {
            closeBehavior = true;
            command.setText("Close");
        }
    }

    @FXML
    protected void onActionShowCFGButton() {
        try {
            String file = "output/" + pHandler.getAbsyn().getName().getId() + "_cfg";
            String dotFile = file + ".dot";
            String pdfFile = file + ".pdf";

            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec("dot -Tpdf " + dotFile + " -o " + pdfFile);
            pr.waitFor();

            new Thread(() -> {
                try {
                    File cfgPDF = new File(pdfFile);
                    Desktop d = Desktop.getDesktop();
                    d.open(cfgPDF);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (InterruptedException e) {
            pHandler.asyncErr("Unexpected error: " + e.getMessage(), "InterruptedException error");
        }catch (IOException e) {
            pHandler.asyncErr("Unexpected error during the .dot file compilation. Make sure this command works in console:\n$> dot -?", "Generation error");
        }
    }

    @FXML
    protected void onActionDominatorsButton() {
        try {
            String file = "output/" + pHandler.getAbsyn().getName().getId() + "_doms";
            String dotFile = file + ".dot";
            String pdfFile = file + ".pdf";

            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec("dot -Tpdf " + dotFile + " -o " + pdfFile);
            pr.waitFor();

            new Thread(() -> {
                try {
                    File domPDF = new File(pdfFile);
                    Desktop d = Desktop.getDesktop();
                    d.open(domPDF);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (InterruptedException e) {
            pHandler.asyncErr("Unexpected error: " + e.getMessage(), "InterruptedException error");
        }catch (IOException e) {
            pHandler.asyncErr("Unexpected error during the .dot file compilation. Make sure this command works in console:\n$> dot -?", "Generation error");
        }
    }

    @FXML
    protected void onActionDominatorsTreeButton() {
        try {
            String file = "output/" + pHandler.getAbsyn().getName().getId() + "_domtree";
            String dotFile = file + ".dot";
            String pdfFile = file + ".pdf";

            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec("dot -Tpdf " + dotFile + " -o " + pdfFile);
            pr.waitFor();

            new Thread(() -> {
                try {
                    File domtreePDF = new File(pdfFile);
                    Desktop d = Desktop.getDesktop();
                    d.open(domtreePDF);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (InterruptedException e) {
            pHandler.asyncErr("Unexpected error: " + e.getMessage(), "InterruptedException error");
        }catch (IOException e) {
            pHandler.asyncErr("Unexpected error during the .dot file compilation. Make sure this command works in console:\n$> dot -?", "Generation error");
        }
    }

    @FXML
    protected void onActionDominanceFrontiersButton() {
        try {
            String file = "output/" + pHandler.getAbsyn().getName().getId() + "_frontiers";
            String dotFile = file + ".dot";
            String pdfFile = file + ".pdf";

            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec("dot -Tpdf " + dotFile + " -o " + pdfFile);
            pr.waitFor();

            new Thread(() -> {
                try {
                    File frontPDF = new File(pdfFile);
                    Desktop d = Desktop.getDesktop();
                    d.open(frontPDF);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (InterruptedException e) {
            pHandler.asyncErr("Unexpected error: " + e.getMessage(), "InterruptedException error");
        }catch (IOException e) {
            pHandler.asyncErr("Unexpected error during the .dot file compilation. Make sure this command works in console:\n$> dot -?", "Generation error");
        }
    }

    private void loadContent() {
        content.setManaged(true);
        content.setVisible(true);
    }
}
