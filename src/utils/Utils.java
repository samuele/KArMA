package utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static controller.ErrorsHandler.err;

public class Utils {

    public static void checkFilename(String filename) {
        if (filename == null || filename.equals("")) {
            err("You must specify a While file to compile.");
            return;
        }

        Pattern p = Pattern.compile("[a-zA-Z_][a-zA-Z0-9_]*\\.while");
        Matcher m = p.matcher(filename);

        if (!m.find()) {
            err("Filename must match the following regex: [a-zA-Z_][a-zA-Z0-9_]*.while");
            return;
        }

        if (filename.endsWith("_ssa.while"))
            err("Filename must not ends with '_ssa.while' suffix.");
    }

    public static String toLex(String filename) {
        String tokens[] = filename.split("/");
        filename = tokens[tokens.length - 1];

        return "output/" + filename.substring(0, filename.length() - ".while".length()) + ".lex";
    }

    public static String toMon(String filename) {
        return filename.substring(0, filename.length() - ".while".length()) + ".mon";
    }

    public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new ArrayList<>(c);
        java.util.Collections.sort(list);
        return list;
    }

    public static String readFile(String path, Charset encoding) throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
