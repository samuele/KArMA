package sqli.lang.syntactical;

class SyntaxErrorException extends SyntacticalAnalyzerException {

    SyntaxErrorException(String message) {
        super(message);
    }
}
