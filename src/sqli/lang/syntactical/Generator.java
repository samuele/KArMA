package sqli.lang.syntactical;

import java_cup.Main;

class Generator {

    private static final String grammarSpecification = "resources/While.cup";
    private static final String cupArgv[] = {
            "-destdir", "src/sqli/lang/syntactical",    // Write the generated files in this directory.
            "-parser", "Parser",                        // The name of the parser class.
            "-symbols", "sym",                          // The name of the class of the symbol constant codes.
            "-interface",                               // Outputs the symbol constant code as an interface rather than as
                                                        // a class.
            "-progress",                                // Print short messages indicating its progress through various
                                                        // parts of the parser generation process.
            "-dump",                                    // Produce a human readable dump of the grammar, the constructed
                                                        // parse states, and the parse tables.
            "-time",                                    // Display time statistics.
            grammarSpecification
    };

    public static void main(String[] args) {
        try {
            Main.main(cupArgv);
        } catch (Exception e) {
            System.err.println("Exception during parser generation: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
