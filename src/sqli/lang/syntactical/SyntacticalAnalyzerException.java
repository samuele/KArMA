package sqli.lang.syntactical;

abstract class SyntacticalAnalyzerException extends RuntimeException { // RuntimeException due to library.

    SyntacticalAnalyzerException(String message) {
        super(message);
    }
}
