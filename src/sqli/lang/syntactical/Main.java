package sqli.lang.syntactical;

import dot.Dot;
import java_cup.runtime.Symbol;
import sqli.lang.absyn.ProgDefinition;
import sqli.lang.lexical.Lexer;
import sqli.lang.lexical.LexicalAnalyzerException;

import java.io.IOException;

import static controller.ErrorsHandler.err;
import static controller.ErrorsHandler.info;

public class Main {

    public static ProgDefinition runSyntacticalAnalysis(Lexer lexer) {
        Parser parser = new Parser(lexer);
        Symbol symbol;

        try {
            symbol = parser.parse();
        } catch (LexicalAnalyzerException e) {
            err("Lexical error", e);
            return null;
        } catch (SyntacticalAnalyzerException e) {
            err("Syntactical error", e);
            return null;
        } catch (Exception e) {
            err("Unexpected error", e);
            return null;
        }

        info("End of the syntactical analysis");

        ProgDefinition absyn = (ProgDefinition) symbol.value;
        String dotName = "output/" + absyn.getName().getId() + "_absyn.dot";

        try (Dot dot = new Dot(dotName)) {
            absyn.toDot(dot);
        } catch (IOException e) {
            err("I/O error", e);
            return null;
        }

        info("Abstract syntax saved into " + dotName);

        return absyn;
    }
}
