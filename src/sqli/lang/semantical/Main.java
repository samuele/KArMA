package sqli.lang.semantical;

import dot.Dot;
import sqli.lang.absyn.ProgDefinition;

import java.io.IOException;

import static controller.ErrorsHandler.err;
import static controller.ErrorsHandler.info;

public class Main {

    public static void runSemanticalAnalysis(ProgDefinition absyn) {
        try {
            absyn.typeCheck(new StaticEnv());
        } catch (SemanticalAnalyzerException e) {
            err("Semantical error", e);
            return;
        }

        info("End of the semantical analysis");

        String progName = absyn.getName().getId();
        String dotName = "output/" + progName + "_types.dot";

        try (Dot dot = new Dot(dotName)) {
            absyn.toDot(dot);
        } catch (IOException e) {
            err("I/O error", e);
            return;
        }

        info("Program " + progName + " is well typed. Type checking result saved into " + dotName);
    }
}
