package sqli.lang.semantical;

public class VariableAlreadyDeclaredException extends SemanticalAnalyzerException {

    public VariableAlreadyDeclaredException(String message) {
        super(message);
    }
}
