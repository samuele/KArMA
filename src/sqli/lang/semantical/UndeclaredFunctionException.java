package sqli.lang.semantical;

public class UndeclaredFunctionException extends SemanticalAnalyzerException {

    public UndeclaredFunctionException(String message) {
        super(message);
    }
}
