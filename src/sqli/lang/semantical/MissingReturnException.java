package sqli.lang.semantical;

public class MissingReturnException extends SemanticalAnalyzerException {

    public MissingReturnException(String message) {
        super(message);
    }
}
