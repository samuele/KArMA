package sqli.lang.semantical;

public class FunctionAlreadyDeclaredException extends SemanticalAnalyzerException {

    public FunctionAlreadyDeclaredException(String message) {
        super(message);
    }
}
