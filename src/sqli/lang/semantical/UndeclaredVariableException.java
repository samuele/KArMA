package sqli.lang.semantical;

public class UndeclaredVariableException extends SemanticalAnalyzerException {

    public UndeclaredVariableException(String message) {
        super(message);
    }
}
