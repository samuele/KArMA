package sqli.lang.semantical;

public class UndefinedPrototypeException extends SemanticalAnalyzerException {

    public UndefinedPrototypeException(String message) {
        super(message);
    }
}
