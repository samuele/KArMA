package sqli.lang.semantical;

public class InvalidReturnStatementException extends SemanticalAnalyzerException {

    public InvalidReturnStatementException(String message) {
        super(message);
    }
}
