package sqli.lang.semantical;

public class ExecArgumentException extends SemanticalAnalyzerException {

    public ExecArgumentException(String message) {
        super(message);
    }
}
