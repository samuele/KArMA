package sqli.lang.semantical;

public class TypeMismatchException extends SemanticalAnalyzerException {

    public TypeMismatchException(String message) {
        super(message);
    }
}
