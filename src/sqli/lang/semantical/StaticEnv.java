package sqli.lang.semantical;

import sqli.lang.absyn.FunctionDeclaration;
import sqli.lang.absyn.FunctionPrototype;
import sqli.lang.absyn.VariableDeclaration;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.VoidType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class StaticEnv {

    private static final Map<String, FunctionPrototype> protsDeclared = new HashMap<>();
    private static final Map<String, FunctionDeclaration> funsDeclared = new HashMap<>();

    private final Map<String, VariableDeclaration> varsDeclared;
    private final PrimitiveType returnType;

    public StaticEnv(PrimitiveType returnType) {
        this.returnType = returnType;
        varsDeclared = new HashMap<>();
    }

    public StaticEnv() {
        this(VoidType.INSTANCE);
    }

    public PrimitiveType getReturnType() {
        return returnType;
    }

    public void addVarDeclaration(VariableDeclaration varDecl) {
        varsDeclared.put(varDecl.getVar().getId(), varDecl);
    }

    public void addFunDeclaration(FunctionDeclaration funDecl) {
        funsDeclared.put(funDecl.getFun().getId(), funDecl);
    }

    public void removeFunDeclaration(FunctionDeclaration decl) {
        funsDeclared.remove(decl.getFun().getId());
    }

    public void addProtDeclaration(FunctionPrototype funProt) {
        protsDeclared.put(funProt.getDecl().getFun().getId(), funProt);
    }

    public boolean isVarDeclared(String var) {
        return varsDeclared.containsKey(var);
    }

    public boolean isFunDeclaredOrPrototyped(String fun) {
        return funsDeclared.containsKey(fun) || protsDeclared.containsKey(fun);
    }

    public boolean isFunDeclared(String fun) {
        return funsDeclared.containsKey(fun);
    }

    public VariableDeclaration getVarDeclaration(String var) {
        return varsDeclared.get(var);
    }

    public FunctionDeclaration getFunDeclaration(String fun) {
        FunctionDeclaration funDecl = funsDeclared.get(fun);
        return funDecl == null ? protsDeclared.get(fun).getDecl() : funDecl;
    }

    public Set<String> getVarsKeySet() {
        return varsDeclared.keySet();
    }

    public Set<FunctionPrototype> getUndefinedPrototypes() {
        Set<FunctionPrototype> undefinedPrototypes = new HashSet<>();
        //noinspection Convert2streamapi
        for (String prot : protsDeclared.keySet())
            if (!funsDeclared.containsKey(prot))
                undefinedPrototypes.add(protsDeclared.get(prot));

        return undefinedPrototypes;
    }
}
