package sqli.lang.semantical;

public class ExecMisplacedException extends SemanticalAnalyzerException {

    public ExecMisplacedException(String message) {
        super(message);
    }
}
