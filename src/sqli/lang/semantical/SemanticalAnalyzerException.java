package sqli.lang.semantical;

public abstract class SemanticalAnalyzerException extends Exception {

    SemanticalAnalyzerException(String message) {
        super(message);
    }
}
