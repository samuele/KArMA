package sqli.lang.semantical;

public class MismatchingPrototypeException extends SemanticalAnalyzerException {

    public MismatchingPrototypeException(String message) {
        super(message);
    }
}
