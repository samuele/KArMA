package sqli.lang.lexical;

class UnmatchedInputException extends LexicalAnalyzerException {

    UnmatchedInputException(String message) {
        super(message);
    }
}
