package sqli.lang.lexical;

import jflex.Main;

class Generator {

    private static final String languageSpecification = "resources/While.flex";
    private static final String jflexArgv[] = {
            "-d", "src/sqli/lang/lexical",    // Write the generated files in this directory.
            "-q",                             // Display error messages only.
            "--time",                         // Display time statistics.
            "--dump",                         // Display transition tables of automata.
            "--warn-unused",                  // Warn about unused macros.
            languageSpecification
    };

    public static void main(String[] args) {
        Main.main(jflexArgv);
    }
}
