package sqli.lang.lexical;

import java_cup.runtime.Symbol;
import sqli.lang.syntactical.sym;

import java.io.FileWriter;
import java.io.IOException;

import static controller.ErrorsHandler.err;
import static controller.ErrorsHandler.info;
import static utils.Utils.toLex;

public class Main {

    public static void runLexicalAnalysis(String fileName, Lexer lexer) {
        if (lexer == null)
            return;

        String lexName = toLex(fileName);
        FileWriter where;

        try {
            where = new FileWriter(lexName);
        } catch (IOException e) {
            err("I/O error", e);
            return;
        }

        do {
            try {
                Symbol tok = lexer.next_token();

                if (tok.sym == sym.EOF)
                    break;

                where.write(tok.sym + ":\t " + sym.terminalNames[tok.sym]);

                Object value = tok.value;
                if (value != null)
                    where.write("(" + value + ")");

                where.write(" @" + tok.left + ":" + tok.right + "\n");
            } catch (LexicalAnalyzerException e) {
                closeDueToError(where);
                err("Lexical error", e);
                return;
            } catch (IOException e) {
                closeDueToError(where);
                err("I/O error", e);
                return;
            }
        }
        while (true);

        try {
            where.close();
        } catch (IOException e) {
            err("I/O error", e);
            return;
        }

        info("End of the lexical analysis.");
        info("Lexical sequence of tokens saved into " + lexName);
    }

    private static void closeDueToError(FileWriter where) {
        //noinspection EmptyCatchBlock
        try {
            where.close();
        } catch (IOException e) { }
    }
}
