package sqli.lang.lexical;

public abstract class LexicalAnalyzerException extends Exception { // public because it is used by the parser.

    LexicalAnalyzerException(String message) {
        super(message);
    }
}
