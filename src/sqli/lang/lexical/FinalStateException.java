package sqli.lang.lexical;

class FinalStateException extends LexicalAnalyzerException {

    FinalStateException(String message) {
        super(message);
    }
}
