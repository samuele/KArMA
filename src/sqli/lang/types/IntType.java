package sqli.lang.types;

public class IntType extends ArithmeticType {

    public static final IntType INSTANCE = new IntType();

    private IntType() { }

    @Override
    public String toString() {
        return "int";
    }

    @Override
    public int hashCode() {
        return "int".hashCode();
    }
}
