package sqli.lang.types;

public class VoidType extends PrimitiveType {

    public static final VoidType INSTANCE = new VoidType();

    private VoidType() { }

    @Override
    public String toString() {
        return "void";
    }

    @Override
    public int hashCode() {
        return "void".hashCode();
    }
}

