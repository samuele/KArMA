package sqli.lang.types;

public class UnitType extends Type {

    public static final UnitType INSTANCE = new UnitType();

    private UnitType() { }

    @Override
    public String toString() {
        return "unit";
    }

    @Override
    public int hashCode() {
        return "unit".hashCode();
    }
}
