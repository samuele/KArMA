package sqli.lang.types;

public class FloatType extends ArithmeticType {

    public static final FloatType INSTANCE = new FloatType();

    private FloatType() { }

    @Override
    public String toString() {
        return "float";
    }

    @Override
    public int hashCode() {
        return "float".hashCode();
    }
}
