package sqli.lang.types;

public class StrType extends PrimitiveType {

    public static final StrType INSTANCE = new StrType();

    private StrType() { }

    @Override
    public String toString() {
        return "str";
    }

    @Override
    public int hashCode() {
        return "str".hashCode();
    }
}
