package sqli.lang.types;

public class FunctionType extends PrimitiveType {

    private final PrimitiveType returnType;
    private final PrimitiveTypeSeq paramsType;

    public FunctionType(PrimitiveType returnType, PrimitiveTypeSeq paramsType) {
        this.returnType = returnType;
        this.paramsType = paramsType;
    }

    public PrimitiveType getReturnType() {
        return returnType;
    }

    public PrimitiveTypeSeq getParamsType() {
        return paramsType;
    }

    @Override
    public String toString() {
        return paramsType.toString() + " -> " + returnType.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FunctionType that = (FunctionType) o;

        //noinspection SimplifiableIfStatement
        if (!returnType.equals(that.returnType)) return false;
        return paramsType != null ? paramsType.equals(that.paramsType) : that.paramsType == null;
    }

    @Override
    public int hashCode() {
        int result = returnType.hashCode();
        result = 31 * result + (paramsType != null ? paramsType.hashCode() : 0);
        return result;
    }
}
