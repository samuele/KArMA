package sqli.lang.types;

public abstract class Type {

    @Override
    public abstract String toString();

    @Override
    public abstract int hashCode();
}
