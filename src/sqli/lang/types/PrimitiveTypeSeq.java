package sqli.lang.types;

public class PrimitiveTypeSeq extends PrimitiveType {

    private final PrimitiveType t;
    private final PrimitiveTypeSeq next;

    public PrimitiveTypeSeq(PrimitiveType t, PrimitiveTypeSeq next) {
        this.t = t;
        this.next = next;
    }

    @Override
    public String toString() {
        return t.toString() + (next != null ? ", " + next.toString() : "");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrimitiveTypeSeq that = (PrimitiveTypeSeq) o;

        //noinspection SimplifiableIfStatement
        if (!t.equals(that.t)) return false;
        return next != null ? next.equals(that.next) : that.next == null;
    }

    @Override
    public int hashCode() {
        int result = t.hashCode();
        result = 31 * result + (next != null ? next.hashCode() : 0);
        return result;
    }
}
