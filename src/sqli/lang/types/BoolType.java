package sqli.lang.types;

public class BoolType extends PrimitiveType {

    public static final BoolType INSTANCE = new BoolType();

    private BoolType() { }

    @Override
    public String toString() {
        return "bool";
    }

    @Override
    public int hashCode() {
        return "bool".hashCode();
    }
}
