package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.StringValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.StrType;

public class StringLiteral extends Literal {

    private final String value;

    public StringLiteral(int line, int column, String value) {
        super(line, column);
        this.value = value;
    }

    @Override
    public StrType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (StrType) super.typeCheck(env);
    }

    @Override
    protected StrType typeCheckImplementation(StaticEnv env) {
        return StrType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "\"" + value + "\"";
    }

    @Override
    public String label() {
        return super.label() + ": \"" + value + "\"";
    }

    @Override
    public StringValue exec(DynamicEnv env) {
        return new StringValue(value);
    }
}
