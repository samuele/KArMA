package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.lang.semantical.FunctionAlreadyDeclaredException;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.FunctionType;

import java.io.IOException;

public class FunctionPrototype extends Declaration {

    private final FunctionDeclaration decl;
    private final FunctionPrototype next;

    public FunctionPrototype(int line, int column, FunctionDeclaration decl, FunctionPrototype next) {
        super(line, column);
        this.decl = decl;
        this.next = next;
    }

    public FunctionDeclaration getDecl() {
        return decl;
    }

    @Override
    public FunctionType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (FunctionType) super.typeCheck(env);
    }

    @Override
    protected FunctionType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        FunctionName fun = decl.getFun();

        if (env.isFunDeclaredOrPrototyped(fun.getId()))
            throw new FunctionAlreadyDeclaredException("Function " + fun.getId() + " already defined at " +
                    env.getFunDeclaration(fun.getId()));

        FunctionType toRet = decl.typeCheck(env);
        env.removeFunDeclaration(decl);
        env.addProtDeclaration(this);

        return toRet;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), decl.toDot(dot), "prototype");
        return next == null ? this : dot.boldLinkNodeToNode(this, next.toDot(dot), "next");
    }
}
