package sqli.lang.absyn;

import sqli.lang.types.StrType;

public class StrTypeDeclaration extends TypeDeclaration {

    public StrTypeDeclaration(int line, int column) {
        super(line, column);
    }

    @Override
    public StrType toType() {
        return StrType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "str";
    }
}
