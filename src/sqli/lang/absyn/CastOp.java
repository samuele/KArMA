package sqli.lang.absyn;

abstract class CastOp extends UnOp {

    CastOp(int line, int column, Expression exp) {
        super(line, column, exp);
    }
}
