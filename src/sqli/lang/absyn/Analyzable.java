package sqli.lang.absyn;

public interface Analyzable extends Use, ExtendedUse, Def, CFGable, CFGPrintable, Exec { }
