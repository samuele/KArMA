package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.BranchingBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.BranchingValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.BoolType;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.UnitType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class IfThenElse extends Command {

    private final Expression cond;
    private final Command then;
    private final Command _else;

    public IfThenElse(int line, int column, Expression cond, Command then, Command _else) {
        super(line, column);
        this.cond = cond;
        this.then = then;
        this._else = _else;
    }

    public IfThenElse(int line, int column, Expression cond, Command then) {
        this(line, column, cond, then, null);
    }

    @Override
    protected UnitType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType condType = cond.typeCheck(env);

        if (condType != BoolType.INSTANCE)
            throw new TypeMismatchException("Condition " + cond + " of " + this + " has type " + condType
                    + " instead of " + BoolType.INSTANCE);

        then.typeCheck(env);
        if (_else != null)
            _else.typeCheck(env);

        return UnitType.INSTANCE;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>(cond.use());
    }

    @Override
    protected Set<ExtUse> extendedUseImplementation() {
        return new HashSet<>(cond.extendedUse());
    }

    @Override
    protected Set<VariableName> defImplementation() {
        return new HashSet<>();
    }

    @Override
    protected Set<Return> retImplementation() {
        Set<Return> ret = new HashSet<>();
        ret.addAll(then.ret());

        if (_else != null)
            ret.addAll(_else.ret());

        return ret;
    }

    @Override
    public boolean hasReturn() {
        return then.hasReturn() && (_else == null || _else.hasReturn());
    }

    @Override
    public String toCFGString() {
        return cond.toCFGString();
    }

    @Override
    public BranchingBlock toCFGBlock() {
        return (BranchingBlock) super.toCFGBlock();
    }

    @Override
    protected BranchingBlock toCFGBlockImplementation() {
        BranchingBlock condBlock = new BranchingBlock(this);
        BasicBlock thenBlock = then.toCFGBlock();

        condBlock.setYesSucc(thenBlock);

        if (_else != null) {
            BasicBlock elseBlock = _else.toCFGBlock();
            condBlock.setNoSucc(elseBlock);
        }

        return condBlock;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), cond.toDot(dot), "cond");
        dot.linkNodeToNode(this, then.toDot(dot), "then");

        if (_else != null)
            dot.linkNodeToNode(this, _else.toDot(dot), "else");

        return this;
    }

    @Override
    public void removeSSA() {
        cond.removeSSA();
        then.removeSSA();
        if (_else != null)
            _else.removeSSA();
    }

    @Override
    public BranchingValue exec(DynamicEnv env) {
        return new BranchingValue((Boolean) cond.exec(env).getValue());
    }
}
