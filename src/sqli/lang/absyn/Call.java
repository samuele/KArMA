package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.Monitor;
import sqli.analysis.monitor.value.ConcreteValue;
import sqli.analysis.taint.Taintable;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.semantical.UndeclaredFunctionException;
import sqli.lang.types.FunctionType;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.PrimitiveTypeSeq;
import sqli.lang.types.VoidType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static sqli.lang.absyn.ExtendedUse.ExtUse;

public class Call extends Expression implements ExtUse, Taintable {

    private final FunctionName fun;
    private final ActualParameter args;

    public Call(int line, int column, FunctionName fun, ActualParameter args) {
        super(line, column);
        this.fun = fun;
        this.args = args;
    }

    public FunctionName getFun() {
        return fun;
    }

    public ActualParameter getArgs() {
        return args;
    }

    @Override
    public boolean isInstanceOfCall() {
        return true;
    }

    @Override
    protected PrimitiveType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        if (!env.isFunDeclaredOrPrototyped(fun.getId()))
            throw new UndeclaredFunctionException("Function " + fun.getId() + " of " + this + " is never declared");

        PrimitiveTypeSeq argsType = args != null ? args.typeCheck(env) : new PrimitiveTypeSeq(VoidType.INSTANCE, null);

        FunctionType funType = env.getFunDeclaration(fun.getId()).typeCheck(env);
        PrimitiveTypeSeq paramsType = funType.getParamsType();

        if (argsType != paramsType && !argsType.equals(paramsType))
            throw new TypeMismatchException("Formal parameters of function " + fun.getId() + " have type " +
                    paramsType + " but arguments have type " + argsType);

        return funType.getReturnType();
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return args != null ? args.use() : new HashSet<>();
    }

    @Override
    protected Set<Call> callsImplementation() {
        Set<Call> calls = new HashSet<>();
        calls.add(this);

        return calls;
    }

    @Override
    public String toCFGString() {
        return fun.getId() + "(" + (args != null ? args.toCFGString() : "") + ")";
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), fun.toDot(dot), "id");
        return args != null ? dot.linkNodeToNode(this, args.toDot(dot), "args") : this;
    }

    @Override
    public void removeSSA() {
        if (args != null)
            args.removeSSA();
    }

    @Override
    public ConcreteValue<?> exec(DynamicEnv env) {
        FunctionDeclaration callee = this.env.getFunDeclaration(fun.getId());
        ParamDeclaration formals = callee.getParams();
        ActualParameter actuals = args;

        DynamicEnv dynEnv = new DynamicEnv();

        while (formals != null) {
            dynEnv.addEntry(formals.getVarDecl().getVar().getId(), actuals.exec(env));
            formals = formals.getNext();
            actuals = actuals.getNext();
        }

        return new Monitor(callee, dynEnv).run();
    }
}
