package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;

import java.io.IOException;

abstract class Name extends Absyn implements CFGPrintable {

    private final String id;

    Name(int line, int column, String id) {
        super(line, column);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toCFGString() {
        return id;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.linkNodeToSymbol(super.toDot(dot), dot.dotSymbol(id), "name");
    }
}
