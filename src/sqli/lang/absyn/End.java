package sqli.lang.absyn;

import sqli.analysis.cfg.SeqBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.EndValue;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.UnitType;

import java.util.HashSet;
import java.util.Set;

public class End extends Command {

    public End(int line, int column) {
        super(line, column);
    }

    @Override
    public UnitType typeCheckImplementation(StaticEnv env) {
        return UnitType.INSTANCE;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>();
    }

    @Override
    protected Set<ExtUse> extendedUseImplementation() {
        return new HashSet<>();
    }

    @Override
    protected Set<VariableName> defImplementation() {
        return new HashSet<>();
    }

    @Override
    protected Set<Return> retImplementation() {
        return new HashSet<>();
    }

    @Override
    public String toCFGString() {
        return "end";
    }

    @Override
    public SeqBlock toCFGBlock() {
        return (SeqBlock) super.toCFGBlock();
    }

    @Override
    protected SeqBlock toCFGBlockImplementation() {
        return new SeqBlock(this);
    }

    @Override
    public void removeSSA() { }

    @Override
    public EndValue exec(DynamicEnv env) {
        return EndValue.INSTANCE;
    }
}
