package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

abstract class TernOp extends Expression {

    final Expression e1;
    final Expression e2;
    final Expression e3;

    TernOp(int line, int column, Expression e1, Expression e2, Expression e3) {
        super(line, column);
        this.e1 = e1;
        this.e2 = e2;
        this.e3 = e3;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        Set<VariableName> use = new HashSet<>();
        use.addAll(e1.use());
        use.addAll(e2.use());
        use.addAll(e3.use());

        return use;
    }

    @Override
    protected Set<Call> callsImplementation() {
        Set<Call> calls = new HashSet<>();
        calls.addAll(e1.calls());
        calls.addAll(e2.calls());
        calls.addAll(e3.calls());

        return calls;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), e1.toDot(dot), "exp1");
        dot.linkNodeToNode(this, e2.toDot(dot), "exp2");
        return dot.linkNodeToNode(this, e3.toDot(dot), "exp3");
    }

    @Override
    public void removeSSA() {
        e1.removeSSA();
        e2.removeSSA();
        e3.removeSSA();
    }
}
