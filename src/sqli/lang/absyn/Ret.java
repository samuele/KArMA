package sqli.lang.absyn;

import java.util.Set;

interface Ret {

    @SuppressWarnings("unused")
    Set<Return> ret();

    @SuppressWarnings("unused")
    boolean hasReturn();
}
