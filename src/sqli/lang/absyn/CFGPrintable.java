package sqli.lang.absyn;

public interface CFGPrintable {

    @SuppressWarnings("unused")
    String toCFGString();
}
