package sqli.lang.absyn;

import sqli.lang.types.FloatType;

public class FloatTypeDeclaration extends ArithmeticTypeDeclaration {

    public FloatTypeDeclaration(int line, int column) {
        super(line, column);
    }

    @Override
    public FloatType toType() {
        return FloatType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "float";
    }
}
