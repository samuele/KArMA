package sqli.lang.absyn;

import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.BoolType;
import sqli.lang.types.PrimitiveType;

abstract class LogicBinOp extends BooleanBinOp {

    LogicBinOp(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }

    @Override
    protected BoolType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType leftType = left.typeCheck(env);
        PrimitiveType rightType = right.typeCheck(env);

        if (leftType != BoolType.INSTANCE)
            throw new TypeMismatchException("Left operand " + left + " of " + this + " has type " + leftType +
                    " instead of the required type " + BoolType.INSTANCE);

        if (rightType != BoolType.INSTANCE)
            throw new TypeMismatchException("Right operand " + right + " of " + this + " has type " + rightType +
                    " instead of the required type " + BoolType.INSTANCE);

        return BoolType.INSTANCE;
    }
}
