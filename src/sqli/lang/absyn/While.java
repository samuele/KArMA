package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.BranchingBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.BranchingValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.BoolType;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.UnitType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class While extends Command {

    private final Expression cond;
    private final Command body;

    public While(int line, int column, Expression cond, Command body) {
        super(line, column);
        this.cond = cond;
        this.body = body;
    }

    @Override
    protected UnitType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType condType = cond.typeCheck(env);

        if (condType != BoolType.INSTANCE)
            throw new TypeMismatchException("Condition " + cond + " of " + this + " has type " + condType
                    + " instead of " + BoolType.INSTANCE);

        body.typeCheck(env);

        return UnitType.INSTANCE;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>(cond.use());
    }

    @Override
    protected Set<ExtUse> extendedUseImplementation() {
        return new HashSet<>(cond.extendedUse());
    }

    @Override
    protected Set<VariableName> defImplementation() {
        return new HashSet<>();
    }

    @Override
    protected Set<Return> retImplementation() {
        return new HashSet<>(body.ret());
    }

    @Override
    public String toCFGString() {
        return cond.toCFGString();
    }

    @Override
    public BranchingBlock toCFGBlock() {
        return (BranchingBlock) super.toCFGBlock();
    }

    @Override
    protected BranchingBlock toCFGBlockImplementation() {
        BranchingBlock condBlock = new BranchingBlock(this);
        BasicBlock bodyBlock = body.toCFGBlock();
        condBlock.setYesSucc(bodyBlock);

        bodyBlock.completeWith(condBlock);

        return condBlock;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), cond.toDot(dot), "cond");
        return dot.linkNodeToNode(this, body.toDot(dot), "body");
    }

    @Override
    public void removeSSA() {
        cond.removeSSA();
        body.removeSSA();
    }

    @Override
    public BranchingValue exec(DynamicEnv env) {
        return new BranchingValue((Boolean) cond.exec(env).getValue());
    }
}
