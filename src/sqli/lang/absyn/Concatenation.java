package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.StringValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.StrType;

public class Concatenation extends StringBinOp {

    public Concatenation(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }

    @Override
    protected StrType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType leftType = left.typeCheck(env);
        PrimitiveType rightType = right.typeCheck(env);

        if (leftType != StrType.INSTANCE)
            throw new TypeMismatchException("First argument " + left + " of " + this + " has type " + leftType
                    + " instead of the required type " + StrType.INSTANCE);

        if (rightType != StrType.INSTANCE)
            throw new TypeMismatchException("Second argument " + right + " of " + this + " has type " + rightType
                    + " instead of the required type " + StrType.INSTANCE);

        return StrType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "concat(" + left.toCFGString() + ", " + right.toCFGString() + ")";
    }

    @Override
    public StringValue exec(DynamicEnv env) {
        String left = (String) this.left.exec(env).getValue();
        String right = (String) this.right.exec(env).getValue();

        return new StringValue(left + right);
    }
}
