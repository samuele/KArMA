package sqli.lang.absyn;

import controller.ProcessHandler;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.FloatValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.FloatType;

public class UntrustedFloat extends ArithmeticUntrustedOp {

    public UntrustedFloat(int line, int column) {
        super(line, column);
    }

    @Override
    public FloatType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (FloatType) super.typeCheck(env);
    }

    @Override
    protected FloatType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        return FloatType.INSTANCE;
    }

    @Override
    public
    String toCFGString() {
        return "taint_float()";
    }

    @Override
    public FloatValue exec(DynamicEnv env) {
        return new FloatValue(ProcessHandler.getInstance().inputFloat());
    }
}
