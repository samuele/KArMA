package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ConcreteValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.PrimitiveTypeSeq;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ActualParameter extends Expression {

    private final Expression exp;
    private final ActualParameter next;

    public ActualParameter(int line, int column, Expression exp, ActualParameter next) {
        super(line, column);
        this.exp = exp;
        this.next = next;
    }

    public ActualParameter getNext() {
        return next;
    }

    public Expression getArg(int k) {
        return k == 0 ? exp : next.getArg(k - 1);
    }

    @Override
    public PrimitiveTypeSeq typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (PrimitiveTypeSeq) super.typeCheck(env);
    }

    @Override
    protected PrimitiveTypeSeq typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        return new PrimitiveTypeSeq(exp.typeCheck(env), next != null ? next.typeCheck(env) : null);
    }

    @Override
    protected Set<VariableName> useImplementation() {
        Set<VariableName> use = new HashSet<>();
        use.addAll(exp.use());

        if (next != null)
            use.addAll(next.use());

        return use;
    }

    @Override
    protected Set<Call> callsImplementation() {
        return exp.calls();
    }

    @Override
    public String toCFGString() {
        return exp.toCFGString() + (next != null ? ", " + next.toCFGString() : "");
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), exp.toDot(dot), "exp");
        return next != null ? dot.boldLinkNodeToNode(this, next.toDot(dot), "next") : this;
    }

    @Override
    public void removeSSA() {
        exp.removeSSA();
        if (next != null)
            next.removeSSA();
    }

    @Override
    public ConcreteValue<?> exec(DynamicEnv env) {
        return exp.exec(env);
    }
}
