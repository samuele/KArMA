package sqli.lang.absyn;

import controller.ProcessHandler;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.StringValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.StrType;

public class UntrustedStr extends UntrustedOp {

    public UntrustedStr(int line, int column) {
        super(line, column);
    }

    @Override
    public StrType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (StrType) super.typeCheck(env);
    }

    @Override
    protected StrType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        return StrType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "taint_str()";
    }

    @Override
    public StringValue exec(DynamicEnv env) {
        return new StringValue(ProcessHandler.getInstance().inputString());
    }
}
