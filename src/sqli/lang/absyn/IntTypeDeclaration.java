package sqli.lang.absyn;

import sqli.lang.types.IntType;

public class IntTypeDeclaration extends ArithmeticTypeDeclaration {

    public IntTypeDeclaration(int line, int column) {
        super(line, column);
    }

    @Override
    public IntType toType() {
        return IntType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "int";
    }
}
