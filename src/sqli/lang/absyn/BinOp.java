package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

abstract class BinOp extends Expression {

    final Expression left;
    final Expression right;

    BinOp(int line, int column, Expression left, Expression right) {
        super(line, column);
        this.left = left;
        this.right = right;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        Set<VariableName> use = new HashSet<>();
        use.addAll(left.use());
        use.addAll(right.use());

        return use;
    }

    @Override
    protected Set<Call> callsImplementation() {
        Set<Call> calls = new HashSet<>();
        calls.addAll(left.calls());
        calls.addAll(right.calls());

        return calls;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), left.toDot(dot), "left");
        return dot.linkNodeToNode(this, right.toDot(dot), "right");
    }

    @Override
    public void removeSSA() {
        left.removeSSA();
        right.removeSSA();
    }
}
