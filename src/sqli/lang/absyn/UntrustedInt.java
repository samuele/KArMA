package sqli.lang.absyn;

import controller.ProcessHandler;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.IntegerValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.IntType;

public class UntrustedInt extends ArithmeticUntrustedOp {

    public UntrustedInt(int line, int column) {
        super(line, column);
    }

    @Override
    public IntType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (IntType) super.typeCheck(env);
    }

    @Override
    protected IntType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        return IntType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "taint_int()";
    }

    @Override
    public IntegerValue exec(DynamicEnv env) {
        return new IntegerValue(ProcessHandler.getInstance().inputInteger());
    }
}
