package sqli.lang.absyn;

import java.util.Set;

public interface Use extends RemoveSSA {

    @SuppressWarnings("unused")
    Set<VariableName> use();
}
