package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.BasicBlock;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.Type;

import java.io.IOException;

public abstract class SubProgram extends Absyn implements Typeable, CFGable {

    StaticEnv envOwned;

    private Type type;
    private BasicBlock block;


    SubProgram(int line, int column) {
        super(line, column);
    }

    public StaticEnv getEnvOwned() {
        return envOwned;
    }

    @Override
    public Type typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return type != null ? type : (type = typeCheckImplementation(env));
    }

    protected abstract Type typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException;

    @Override
    public BasicBlock toCFGBlock() {
        return block != null ? block : (block = toCFGBlockImplementation());
    }

    protected abstract BasicBlock toCFGBlockImplementation();

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return type != null
                ? dot.linkNodeToSymbol(super.toDot(dot), dot.dotSymbol(type.toString()), "type")
                : super.toDot(dot);
    }
}
