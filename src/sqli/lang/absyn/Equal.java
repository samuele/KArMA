package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.BooleanValue;

public class Equal extends SameTypeComparisonBinOp {

    public Equal(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }

    @Override
    public String toCFGString() {
        return "(" + left.toCFGString() + " == " + right.toCFGString() + ")";
    }

    @Override
    public BooleanValue exec(DynamicEnv env) {
        return new BooleanValue(left.exec(env).getValue().equals(right.exec(env).getValue()));
    }
}
