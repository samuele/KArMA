package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.IntegerValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.IntType;

public class IntegerLiteral extends ArithmeticLiteral {

    private final int value;

    public IntegerLiteral(int line, int column, int value) {
        super(line, column);
        this.value = value;
    }

    @Override
    public IntType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (IntType) super.typeCheck(env);
    }

    @Override
    protected IntType typeCheckImplementation(StaticEnv env) {
        return IntType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return String.valueOf(value);
    }

    @Override
    public String label() {
        return super.label() + ": " + value;
    }

    @Override
    public IntegerValue exec(DynamicEnv env) {
        return new IntegerValue(value);
    }
}
