package sqli.lang.absyn;

import sqli.analysis.cfg.BasicBlock;

public interface CFGable {

    @SuppressWarnings("unused")
    BasicBlock toCFGBlock();
}
