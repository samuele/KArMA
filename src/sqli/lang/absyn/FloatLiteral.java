package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.FloatValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.FloatType;

public class FloatLiteral extends ArithmeticLiteral {

    private final float value;

    public FloatLiteral(int line, int column, float value) {
        super(line, column);
        this.value = value;
    }

    @Override
    public FloatType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (FloatType) super.typeCheck(env);
    }

    @Override
    protected FloatType typeCheckImplementation(StaticEnv env) {
        return FloatType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return String.valueOf(value);
    }

    @Override
    public String label() {
        return super.label() + ": " + value;
    }

    @Override
    public FloatValue exec(DynamicEnv env) {
        return new FloatValue(value);
    }
}
