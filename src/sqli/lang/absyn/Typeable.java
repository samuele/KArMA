package sqli.lang.absyn;

import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.Type;

interface Typeable {

    @SuppressWarnings("unused")
    Type typeCheck(StaticEnv env) throws SemanticalAnalyzerException;
}
