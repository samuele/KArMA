package sqli.lang.absyn;

import java.util.HashSet;
import java.util.Set;

abstract class Literal extends Expression {

    Literal(int line, int column) {
        super(line, column);
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>();
    }

    @Override
    protected Set<Call> callsImplementation() {
        return new HashSet<>();
    }

    @Override
    public void removeSSA() { }
}
