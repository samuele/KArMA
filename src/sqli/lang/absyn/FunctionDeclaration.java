package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.lang.semantical.FunctionAlreadyDeclaredException;
import sqli.lang.semantical.MismatchingPrototypeException;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.FunctionType;
import sqli.lang.types.PrimitiveTypeSeq;
import sqli.lang.types.VoidType;

import java.io.IOException;

public class FunctionDeclaration extends Declaration {

    private final TypeDeclaration typeDeclaration;
    private final FunctionName fun;
    private final ParamDeclaration params;

    private StaticEnv newEnv;

    public FunctionDeclaration(int line, int column, TypeDeclaration typeDeclaration, FunctionName fun,
                               ParamDeclaration params) {
        super(line, column);
        this.typeDeclaration = typeDeclaration;
        this.fun = fun;
        this.params = params;
    }

    public FunctionName getFun() {
        return fun;
    }

    public ParamDeclaration getParams() {
        return params;
    }

    StaticEnv getNewEnv() {
        return newEnv;
    }

    @Override
    public FunctionType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        if (type != null)
            return (FunctionType) type;

        return (FunctionType) (type = typeCheckImplementation(newEnv = new StaticEnv(typeDeclaration.toType())));
    }

    @Override
    protected FunctionType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        if (env.isFunDeclared(fun.getId()))
            throw new FunctionAlreadyDeclaredException("Function " + fun.getId() + " already defined at "
                    + env.getFunDeclaration(fun.getId()));

        PrimitiveTypeSeq paramsType = new PrimitiveTypeSeq(VoidType.INSTANCE, null);
        if (params != null)
            paramsType = params.typeCheck(env);

        FunctionType toRet = new FunctionType(typeDeclaration.toType(), paramsType);

        if (env.isFunDeclaredOrPrototyped(fun.getId())) {
            FunctionDeclaration protDecl = env.getFunDeclaration(fun.getId());
            FunctionType protType = protDecl.typeCheck(env);

            if (!protType.equals(toRet))
                throw new MismatchingPrototypeException("Function " + fun.getId() + " has type " + toRet
                        + " but was declared with type " + protType);

            if (!protDecl.params.equals(params))
                throw new MismatchingPrototypeException("Function " + fun.getId() + " has params " + params
                        + " but was declared with params " + protDecl.params);
        }

        env.addFunDeclaration(this);

        return toRet;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), typeDeclaration.toDot(dot), "return type");
        dot.linkNodeToNode(this, fun.toDot(dot), "id");
        return params != null ? dot.linkNodeToNode(this, params.toDot(dot), "params") : this;
    }
}
