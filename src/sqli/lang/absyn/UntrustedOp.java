package sqli.lang.absyn;

import java.util.HashSet;
import java.util.Set;

public abstract class UntrustedOp extends Expression {

    UntrustedOp(int line, int column) {
        super(line, column);
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>();
    }

    @Override
    protected Set<Call> callsImplementation() {
        return new HashSet<>();
    }

    @Override
    public void removeSSA() { }
}
