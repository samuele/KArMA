package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.BooleanValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.BoolType;

abstract class BooleanUnOp extends UnOp {

    BooleanUnOp(int line, int column, Expression exp) {
        super(line, column, exp);
    }

    @Override
    public BoolType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (BoolType) super.typeCheck(env);
    }

    @Override
    protected abstract BoolType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException;

    @Override
    public abstract BooleanValue exec(DynamicEnv env);
}
