package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;

import java.io.IOException;

abstract class Absyn implements DotableNode {

    private final int line;
    private final int column;

    private String nodeName;

    Absyn(int line, int column) {
        this.line = line;
        this.column = column;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.dotNode(this);
    }

    @Override
    public String getNodeName() {
        return nodeName;
    }

    @Override
    public String setNodeName(String nodeName) {
        return this.nodeName = nodeName;
    }

    @Override
    public String label() {
        return getClass().getSimpleName();
    }

    @Override
    public String toString() {
        return "[" + label() + " @" + line + ":" + column + "]";
    }
}
