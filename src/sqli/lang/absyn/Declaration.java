package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.Type;

import java.io.IOException;

abstract class Declaration extends Absyn implements Typeable {

    Type type;

    Declaration(int line, int column) {
        super(line, column);
    }

    @Override
    public Type typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return type != null ? type : (type = typeCheckImplementation(env));
    }

    protected abstract Type typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException;

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return type != null
                ? dot.linkNodeToSymbol(super.toDot(dot), dot.dotSymbol(type.toString()), "type")
                : super.toDot(dot);
    }
}
