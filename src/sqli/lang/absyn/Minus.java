package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ArithmeticValue;
import sqli.analysis.monitor.value.FloatValue;
import sqli.analysis.monitor.value.IntegerValue;
import sqli.analysis.monitor.value.Value;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.ArithmeticType;
import sqli.lang.types.FloatType;
import sqli.lang.types.IntType;
import sqli.lang.types.PrimitiveType;

public class Minus extends ArithmeticUnOp {

    public Minus(int line, int column, Expression exp) {
        super(line, column, exp);
    }

    @Override
    protected ArithmeticType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType expType = exp.typeCheck(env);

        if (expType != IntType.INSTANCE && expType != FloatType.INSTANCE)
            throw new TypeMismatchException("Operand " + exp + " of " + this + " has type " + expType
                    + " instead of the required type " + IntType.INSTANCE + " or " + FloatType.INSTANCE);

        return (ArithmeticType) expType;
    }

    @Override
    public String toCFGString() {
        return "-(" + exp.toCFGString() + ")";
    }

    @Override
    public ArithmeticValue<?> exec(DynamicEnv env) {
        Value<?> expValue = exp.exec(env);

        if (expValue instanceof IntegerValue)
            return new IntegerValue((Integer) expValue.getValue());

        return new FloatValue((Float) expValue.getValue());
    }
}
