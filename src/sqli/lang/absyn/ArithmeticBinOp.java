package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ArithmeticValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.ArithmeticType;
import sqli.lang.types.FloatType;
import sqli.lang.types.IntType;
import sqli.lang.types.PrimitiveType;

abstract class ArithmeticBinOp extends BinOp {

    ArithmeticBinOp(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }

    @Override
    public ArithmeticType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (ArithmeticType) super.typeCheck(env);
    }

    @Override
    protected ArithmeticType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType leftType = left.typeCheck(env);
        PrimitiveType rightType = right.typeCheck(env);

        if (leftType != IntType.INSTANCE && leftType != FloatType.INSTANCE)
            throw new TypeMismatchException("Left operand " + left + " of " + this + " has type " + leftType
                    + " instead of the required type " + IntType.INSTANCE + " or " + FloatType.INSTANCE);

        if (rightType != IntType.INSTANCE && rightType != FloatType.INSTANCE)
            throw new TypeMismatchException("Right operand " + right + " of " + this + " has type " + rightType
                    + " instead of the required type " + IntType.INSTANCE + " or " + FloatType.INSTANCE);

        return ((leftType == rightType) && (leftType == IntType.INSTANCE)) ? IntType.INSTANCE : FloatType.INSTANCE;
    }

    @Override
    public abstract ArithmeticValue<?> exec(DynamicEnv env);
}
