package sqli.lang.absyn;

import sqli.lang.types.ArithmeticType;

abstract class ArithmeticTypeDeclaration extends TypeDeclaration {

    ArithmeticTypeDeclaration(int line, int column) {
        super(line, column);
    }

    @Override
    public abstract ArithmeticType toType();
}
