package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.Value;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.UnitType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class CommandSeq extends Command {

    private final Command first;
    private final Command second;

    public CommandSeq(int line, int column, Command first, Command second) {
        super(line, column);
        this.first = first;
        this.second = second;
    }

    @Override
    protected UnitType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        first.typeCheck(env);
        second.typeCheck(env);

        return UnitType.INSTANCE;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>(first.use());
    }

    @Override
    protected Set<ExtUse> extendedUseImplementation() {
        return new HashSet<>(first.extendedUse());
    }

    @Override
    protected Set<VariableName> defImplementation() {
        return new HashSet<>(first.def());
    }

    @Override
    protected Set<Return> retImplementation() {
        Set<Return> ret = new HashSet<>();
        ret.addAll(first.ret());
        ret.addAll(second.ret());

        return ret;
    }

    @Override
    public String toCFGString() {
        return first.toCFGString();
    }

    @Override
    protected BasicBlock toCFGBlockImplementation() {
        BasicBlock firstBlock = first.toCFGBlock();
        BasicBlock secondBlock = second.toCFGBlock();

        firstBlock.completeWith(secondBlock);

        return firstBlock;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), first.toDot(dot), "first");
        return dot.linkNodeToNode(this, second.toDot(dot), "second");
    }

    @Override
    public void removeSSA() {
        first.removeSSA();
        second.removeSSA();
    }

    @Override
    public Value<?> exec(DynamicEnv env) {
        return first.exec(env);
    }
}
