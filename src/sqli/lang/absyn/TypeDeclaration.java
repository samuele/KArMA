package sqli.lang.absyn;

import sqli.lang.types.PrimitiveType;

public abstract class TypeDeclaration extends Absyn implements CFGPrintable {

    TypeDeclaration(int line, int column) {
        super(line, column);
    }

    public abstract PrimitiveType toType();
}
