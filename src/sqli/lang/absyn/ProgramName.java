package sqli.lang.absyn;

public class ProgramName extends Name {

    public ProgramName(int line, int column, String id) {
        super(line, column, id);
    }
}
