package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ConcreteValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.PrimitiveType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public abstract class Expression extends Absyn implements Typeable, Use, ExtendedUse, CFGPrintable, Exec {

    StaticEnv env;
    private PrimitiveType type;
    private Set<VariableName> use;
    private Set<ExtUse> extendedUse;
    private Set<Call> calls;

    Expression(int line, int column) {
        super(line, column);
    }

    @Override
    public PrimitiveType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        if (type != null)
            return type;

        this.env = env;
        return type = typeCheckImplementation(env);
    }

    protected abstract PrimitiveType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException;

    @Override
    public Set<VariableName> use() {
        return use != null ? use : (use = useImplementation());
    }

    protected abstract Set<VariableName> useImplementation();

    @Override
    public Set<ExtUse> extendedUse() {
        if (extendedUse != null)
            return extendedUse;

        extendedUse = new HashSet<>(use());
        Set<Call> calls = calls();

        for (Call call : calls)
            extendedUse.removeAll(call.use());

        extendedUse.addAll(calls);

        return extendedUse;
    }

    Set<Call> calls() {
        return calls != null ? calls : (calls = callsImplementation());
    }

    protected abstract Set<Call> callsImplementation();

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return type != null
                ? dot.linkNodeToSymbol(super.toDot(dot), dot.dotSymbol(type.toString()), "type")
                : super.toDot(dot);
    }

    @Override
    public abstract ConcreteValue<?> exec(DynamicEnv env);
}
