package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ConcreteValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.UndeclaredVariableException;
import sqli.lang.types.PrimitiveType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Deref extends Expression {

    private final VariableName var;

    public Deref(int line, int column, VariableName var) {
        super(line, column);
        this.var = var;
    }

    @Override
    protected PrimitiveType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        if (!env.isVarDeclared(var.getId()))
            throw new UndeclaredVariableException("Variable " + var.getId() + " of " + this + " is never declared");

        return env.getVarDeclaration(var.getId()).getTypeDeclaration().toType();
    }

    @Override
    protected Set<VariableName> useImplementation() {
        Set<VariableName> use = new HashSet<>();
        use.add(var);

        return use;
    }

    @Override
    protected Set<Call> callsImplementation() {
        return new HashSet<>();
    }

    @Override
    public String toCFGString() {
        return var.toCFGString();
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.linkNodeToNode(super.toDot(dot), var.toDot(dot), "var");
    }

    @Override
    public void removeSSA() {
        var.removeSSA();
    }

    @Override
    public ConcreteValue<?> exec(DynamicEnv env) {
        return env.getValue(var.getId());
    }
}
