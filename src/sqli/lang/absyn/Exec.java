package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.Value;

public interface Exec {

    Value<?> exec(DynamicEnv env);
}
