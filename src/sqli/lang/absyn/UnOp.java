package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;

import java.io.IOException;
import java.util.Set;

abstract class UnOp extends Expression {

    final Expression exp;

    UnOp(int line, int column, Expression exp) {
        super(line, column);
        this.exp = exp;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return exp.use();
    }

    @Override
    protected Set<Call> callsImplementation() {
        return exp.calls();
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.linkNodeToNode(super.toDot(dot), exp.toDot(dot), "exp");
    }

    @Override
    public void removeSSA() {
        exp.removeSSA();
    }
}
