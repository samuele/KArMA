package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.SeqBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ConcreteValue;
import sqli.analysis.monitor.value.UnitValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.UnitType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class VariableDefinition extends Command {

    private final VariableDeclaration decl;
    private final Expression exp;
    private final VariableDefinition next;

    public VariableDefinition(int line, int column, VariableDeclaration decl, Expression exp, VariableDefinition next) {
        super(line, column);
        this.decl = decl;
        this.exp = exp;
        this.next = next;
    }

    public VariableDeclaration getDecl() {
        return decl;
    }

    public Expression getExp() {
        return exp;
    }

    @Override
    protected UnitType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType declType = decl.typeCheck(env);
        PrimitiveType expType = exp.typeCheck(env);

        VariableName var = decl.getVar();

        if (expType != declType)
            throw new TypeMismatchException("Variable " + var.getId() + " has type " + declType + " but expression "
                    + exp + " has type " + expType);

        if (next != null)
            next.typeCheck(env);

        return UnitType.INSTANCE;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>(exp.use());
    }

    @Override
    protected Set<ExtUse> extendedUseImplementation() {
        return new HashSet<>(exp.extendedUse());
    }

    @Override
    protected Set<VariableName> defImplementation() {
        return new HashSet<>(decl.def());
    }

    @Override
    protected Set<Return> retImplementation() {
        return new HashSet<>();
    }

    @Override
    public String toCFGString() {
        return decl.toCFGString() + " := " + exp.toCFGString();
    }

    @Override
    public SeqBlock toCFGBlock() {
        return (SeqBlock) super.toCFGBlock();
    }

    @Override
    protected SeqBlock toCFGBlockImplementation() {
        SeqBlock varBlock = new SeqBlock(this);

        if (next != null) {
            SeqBlock nextBlock = next.toCFGBlock();
            varBlock.setSucc(nextBlock);
        }

        return varBlock;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), decl.toDot(dot), "decl");
        dot.linkNodeToNode(this, exp.toDot(dot), "exp");
        return next != null ? dot.boldLinkNodeToNode(this, next.toDot(dot), "next") : this;
    }

    @Override
    public void removeSSA() {
        decl.removeSSA();
        exp.removeSSA();
        if (next != null)
            next.removeSSA();
    }

    @Override
    public UnitValue exec(DynamicEnv env) {
        ConcreteValue<?> val = exp.exec(env);
        env.addEntry(decl.getVar().getId(), val);

        if (exp instanceof UntrustedOp)
            env.addUntrustedValue(decl.getVar().getId(), val);

        return UnitValue.INSTANCE;
    }
}
