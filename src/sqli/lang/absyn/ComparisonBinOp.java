package sqli.lang.absyn;

abstract class ComparisonBinOp extends BooleanBinOp {

    ComparisonBinOp(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }
}
