package sqli.lang.absyn;

import sqli.lang.types.BoolType;

public class BoolTypeDeclaration extends TypeDeclaration {

    public BoolTypeDeclaration(int line, int column) {
        super(line, column);
    }

    @Override
    public BoolType toType() {
        return BoolType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "bool";
    }
}
