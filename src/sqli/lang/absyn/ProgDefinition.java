package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.SeqBlock;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.UndefinedPrototypeException;
import sqli.lang.types.UnitType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ProgDefinition extends SubProgram {

    private final ProgramName name;
    private final FunctionPrototype prototypes;
    private final FunctionDefinition functions;
    private final VariableDefinition variables;
    private final Command com;


    public ProgDefinition(int line, int column, ProgramName name, FunctionPrototype prototypes,
                          FunctionDefinition functions, VariableDefinition variables, Command com) {
        super(line, column);
        this.name = name;
        this.prototypes = prototypes;
        this.functions = functions;
        this.variables = variables;
        this.com = com;
    }

    public ProgramName getName() {
        return name;
    }

    public Set<FunctionDefinition> collectFunctionDefinitions() {
        if (functions == null)
            return new HashSet<>();

        return functions.collectFunctionDefinitions();
    }

    @Override
    protected UnitType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        envOwned = env;

        if (prototypes != null)
            prototypes.typeCheck(env);

        if (functions != null)
            functions.typeCheck(env);

        //noinspection LoopStatementThatDoesntLoop
        for (FunctionPrototype prot : env.getUndefinedPrototypes())
            throw new UndefinedPrototypeException("Function " + prot + " declared but not defined");

        if (variables != null)
            variables.typeCheck(env);

        com.typeCheck(env);

        return UnitType.INSTANCE;
    }

    @Override
    protected BasicBlock toCFGBlockImplementation() {
        BasicBlock comBlock = com.toCFGBlock();

        SeqBlock varsBlock = null;
        if (variables != null) {
            varsBlock = variables.toCFGBlock();
            varsBlock.completeWith(comBlock);
        }

        return varsBlock != null ? varsBlock : comBlock;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), name.toDot(dot), "id");

        if (prototypes != null)
            dot.linkNodeToNode(this, prototypes.toDot(dot), "prototype");

        if (functions != null)
            dot.linkNodeToNode(this, functions.toDot(dot), "fun");

        if (variables != null)
            dot.linkNodeToNode(this, variables.toDot(dot), "var");

        return dot.linkNodeToNode(this, com.toDot(dot), "com");
    }
}
