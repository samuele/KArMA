package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.BooleanValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.BoolType;
import sqli.lang.types.PrimitiveType;

public class Not extends BooleanUnOp {

    public Not(int line, int column, Expression exp) {
        super(line, column, exp);
    }

    @Override
    protected BoolType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType expType = exp.typeCheck(env);

        if (expType != BoolType.INSTANCE)
            throw new TypeMismatchException("Operand " + exp + " of " + this + " has type " + expType
                    + " instead of the required type " + BoolType.INSTANCE);

        return BoolType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "!(" + exp.toCFGString() + ")";
    }

    @Override
    public BooleanValue exec(DynamicEnv env) {
        return new BooleanValue(!((Boolean) exp.exec(env).getValue()));
    }
}
