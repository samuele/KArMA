package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.StringValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.IntType;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.StrType;

public class Substring extends StringTernOp {

    public Substring(int line, int column, Expression e1, Expression e2, Expression e3) {
        super(line, column, e1, e2, e3);
    }

    @Override
    protected StrType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType t1 = e1.typeCheck(env);
        PrimitiveType t2 = e2.typeCheck(env);
        PrimitiveType t3 = e3.typeCheck(env);

        if (t1 != StrType.INSTANCE)
            throw new TypeMismatchException("First argument " + e1 + " of " + this + " has type " + t1
                    + " instead of the required type " + StrType.INSTANCE);

        if (t2 != IntType.INSTANCE)
            throw new TypeMismatchException("Second argument " + e2 + " of " + this + " has type " + t2
                    + " instead of the required type " + IntType.INSTANCE);

        if (t3 != IntType.INSTANCE)
            throw new TypeMismatchException("Third argument " + e3 + " of " + this + " has type " + t3
                    + " instead of the required type " + IntType.INSTANCE);

        return StrType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "substr(" + e1.toCFGString() + ", " + e2.toCFGString() + ", " + e3.toCFGString() + ")";
    }

    @Override
    public StringValue exec(DynamicEnv env) {
        String s = (String) e1.exec(env).getValue();
        int beginIndex = (Integer) e2.exec(env).getValue();
        int endIndex = (Integer) e3.exec(env).getValue();

        return new StringValue(s.substring(beginIndex, endIndex));
    }
}
