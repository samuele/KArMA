package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.SeqBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.UnitValue;
import sqli.analysis.monitor.value.Value;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.VariableAlreadyDeclaredException;
import sqli.lang.types.PrimitiveType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class VariableDeclaration extends Declaration implements Ret, Analyzable {

    private final TypeDeclaration typeDeclaration;
    private final VariableName var;

    private Set<VariableName> use;
    private Set<ExtUse> extendedUse;
    private Set<VariableName> def;
    private Set<Return> ret;
    private BasicBlock block;
    private int param;

    public VariableDeclaration(int line, int column, TypeDeclaration typeDeclaration, VariableName var) {
        super(line, column);
        this.typeDeclaration = typeDeclaration;
        this.var = var;
    }

    TypeDeclaration getTypeDeclaration() {
        return typeDeclaration;
    }

    public VariableName getVar() {
        return var;
    }

    public int getParam() {
        return param;
    }

    void incParam() {
        ++param;
    }

    @Override
    public PrimitiveType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (PrimitiveType) super.typeCheck(env);
    }

    @Override
    public PrimitiveType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        if (env.isVarDeclared(var.getId()))
            throw new VariableAlreadyDeclaredException("Variable " + var.getId() + " already defined at "
                    + env.getVarDeclaration(var.getId()));

        env.addVarDeclaration(this);

        return typeDeclaration.toType();
    }

    @Override
    public Set<VariableName> use() {
        return use != null ? use : (use = new HashSet<>());
    }

    @Override
    public Set<ExtUse> extendedUse() {
        return extendedUse != null ? extendedUse : (extendedUse = new HashSet<>());
    }

    @Override
    public Set<VariableName> def() {
        if (def != null)
            return def;

        Set<VariableName> def = new HashSet<>();
        def.add(var);

        return this.def = def;
    }

    @Override
    public Set<Return> ret() {
        return ret != null ? ret : (ret = new HashSet<>());
    }

    @Override
    public boolean hasReturn() {
        return !ret().isEmpty();
    }

    @Override
    public String toCFGString() {
        return typeDeclaration.toCFGString() + " " + var.toCFGString() + (param != 0 ? " (" + param + ")" : "");
    }

    @Override
    public SeqBlock toCFGBlock() {
        return (SeqBlock) (block != null ? block : (block = new SeqBlock(this)));
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), typeDeclaration.toDot(dot), "type");
        return dot.linkNodeToNode(this, var.toDot(dot), "id");
    }

    @Override
    public void removeSSA() {
        var.removeSSA();
    }

    @Override
    public Value<?> exec(DynamicEnv env) {
        return UnitValue.INSTANCE;
    }
}
