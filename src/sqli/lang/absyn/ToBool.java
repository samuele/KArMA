package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.BooleanValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.BoolType;

public class ToBool extends CastOp {

    public ToBool(int line, int column, Expression exp) {
        super(line, column, exp);
    }

    @Override
    public BoolType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (BoolType) super.typeCheck(env);
    }

    @Override
    protected BoolType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        exp.typeCheck(env);
        return BoolType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "tobool(" + exp.toCFGString() + ")";
    }

    @Override
    public BooleanValue exec(DynamicEnv env) {
        return new BooleanValue(Boolean.parseBoolean(exp.exec(env).getValue().toString()));
    }
}
