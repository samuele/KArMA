package sqli.lang.absyn;

import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.BoolType;
import sqli.lang.types.FloatType;
import sqli.lang.types.IntType;
import sqli.lang.types.PrimitiveType;

abstract class ArithmeticComparisonBinOp extends ComparisonBinOp {

    ArithmeticComparisonBinOp(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }

    @Override
    protected BoolType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType leftType = left.typeCheck(env);
        PrimitiveType rightType = right.typeCheck(env);

        if (leftType != IntType.INSTANCE && leftType != FloatType.INSTANCE)
            throw new TypeMismatchException("Left operand " + left + " of " + this + " has type " + leftType
                    + " instead of the required type " + IntType.INSTANCE + " or " + FloatType.INSTANCE);

        if (rightType != IntType.INSTANCE && rightType != FloatType.INSTANCE)
            throw new TypeMismatchException("Right operand " + right + " of " + this + " has type " + rightType
                    + " instead of the required type " + IntType.INSTANCE + " or " + FloatType.INSTANCE);

        return BoolType.INSTANCE;
    }
}
