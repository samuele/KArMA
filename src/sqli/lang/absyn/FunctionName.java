package sqli.lang.absyn;

public class FunctionName extends Name {

    public FunctionName(int line, int column, String id) {
        super(line, column, id);
    }
}
