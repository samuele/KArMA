package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.SeqBlock;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.PrimitiveTypeSeq;

import java.io.IOException;

public class ParamDeclaration extends Declaration implements CFGable {

    private final VariableDeclaration varDecl;
    private final ParamDeclaration next;

    public ParamDeclaration(int line, int column, VariableDeclaration varDecl, ParamDeclaration next) {
        super(line, column);
        this.varDecl = varDecl;
        this.next = next;

        incParam();
    }

    public VariableDeclaration getVarDecl() {
        return varDecl;
    }

    public ParamDeclaration getNext() {
        return next;
    }

    private void incParam() {
        varDecl.incParam();
        if (next != null)
            next.incParam();
    }

    @Override
    public PrimitiveTypeSeq typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (PrimitiveTypeSeq) super.typeCheck(env);
    }

    @Override
    protected PrimitiveTypeSeq typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        return new PrimitiveTypeSeq(varDecl.typeCheck(env), next != null ? next.typeCheck(env) : null);
    }

    @Override
    public SeqBlock toCFGBlock() {
        SeqBlock varBlock = varDecl.toCFGBlock();

        if (next != null)
            varBlock.setSucc(next.toCFGBlock());

        return varBlock;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), varDecl.toDot(dot), "var");
        return next == null ? this : dot.boldLinkNodeToNode(this, next.toDot(dot), "next");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParamDeclaration that = (ParamDeclaration) o;

        if (varDecl.getTypeDeclaration().toType() != that.varDecl.getTypeDeclaration().toType()) return false;
        //noinspection SimplifiableIfStatement
        if (!varDecl.getVar().getId().equals(that.varDecl.getVar().getId())) return false;
        return next != null ? next.equals(that.next) : that.next == null;
    }

    @Override
    public int hashCode() {
        int result = varDecl.getTypeDeclaration().toType().hashCode() + varDecl.getVar().getId().hashCode();
        result = 31 * result + (next != null ? next.hashCode() : 0);
        return result;
    }
}
