package sqli.lang.absyn;

import java.util.Set;

public interface Def extends RemoveSSA {

    @SuppressWarnings("unused")
    Set<VariableName> def();
}
