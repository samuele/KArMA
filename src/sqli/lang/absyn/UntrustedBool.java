package sqli.lang.absyn;

import controller.ProcessHandler;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.BooleanValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.BoolType;

public class UntrustedBool extends UntrustedOp {

    public UntrustedBool(int line, int column) {
        super(line, column);
    }

    @Override
    public BoolType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (BoolType) super.typeCheck(env);
    }

    @Override
    protected BoolType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        return BoolType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "taint_bool()";
    }

    @Override
    public BooleanValue exec(DynamicEnv env) {
        return new BooleanValue(ProcessHandler.getInstance().inputBoolean());
    }
}
