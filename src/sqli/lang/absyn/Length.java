package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.IntegerValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.IntType;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.StrType;

public class Length extends ArithmeticUnOp {

    public Length(int line, int column, Expression exp) {
        super(line, column, exp);
    }

    @Override
    public IntType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (IntType) super.typeCheck(env);
    }

    @Override
    protected IntType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType expType = exp.typeCheck(env);

        if (expType != StrType.INSTANCE)
            throw new TypeMismatchException("Operand " + exp + " of " + this + " has type " + expType
                    + " instead of the required type " + StrType.INSTANCE);

        return IntType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "length(" + exp.toCFGString() + ")";
    }

    @Override
    public IntegerValue exec(DynamicEnv env) {
        return new IntegerValue(((String) exp.exec(env).getValue()).length());
    }
}
