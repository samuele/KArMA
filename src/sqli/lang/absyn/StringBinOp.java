package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.StringValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.StrType;

abstract class StringBinOp extends BinOp {

    StringBinOp(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }

    @Override
    public StrType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (StrType) super.typeCheck(env);
    }

    @Override
    protected abstract StrType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException;

    @Override
    public abstract StringValue exec(DynamicEnv env);
}
