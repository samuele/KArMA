package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.SeqBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.UnitValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.semantical.UndeclaredVariableException;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.UnitType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Assignment extends Command {

    private final VariableName var;
    private final Expression exp;

    public Assignment(int line, int column, VariableName var, Expression exp) {
        super(line, column);
        this.var = var;
        this.exp = exp;
    }

    @Override
    protected UnitType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType expType = exp.typeCheck(env);

        if (!env.isVarDeclared(var.getId()))
            throw new UndeclaredVariableException("Variable " + var + " of " + this + " is never declared");

        PrimitiveType varType = env.getVarDeclaration(var.getId()).typeCheck(env);

        if (expType != varType)
            throw new TypeMismatchException("Variable " + var + " left value of " + this + " has type " + varType
                    + " but the right value " + exp + " has type " + expType);

        return UnitType.INSTANCE;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>(exp.use());
    }

    @Override
    protected Set<ExtUse> extendedUseImplementation() {
        return new HashSet<>(exp.extendedUse());
    }

    @Override
    protected Set<VariableName> defImplementation() {
        Set<VariableName> def = new HashSet<>();
        def.add(var);

        return def;
    }

    @Override
    protected Set<Return> retImplementation() {
        return new HashSet<>();
    }

    @Override
    public String toCFGString() {
        return var.toCFGString() + " := " + exp.toCFGString();
    }

    @Override
    public SeqBlock toCFGBlock() {
        return (SeqBlock) super.toCFGBlock();
    }

    @Override
    protected SeqBlock toCFGBlockImplementation() {
        return new SeqBlock(this);
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), var.toDot(dot), "var");
        return dot.linkNodeToNode(this, exp.toDot(dot), "exp");
    }

    @Override
    public void removeSSA() {
        var.removeSSA();
        exp.removeSSA();
    }

    @Override
    public UnitValue exec(DynamicEnv env) {
        env.addEntry(var.getId(), exp.exec(env));
        return UnitValue.INSTANCE;
    }
}
