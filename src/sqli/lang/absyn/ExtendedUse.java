package sqli.lang.absyn;

import java.util.Set;

public interface ExtendedUse extends RemoveSSA {

    @SuppressWarnings("unused")
    Set<ExtUse> extendedUse();

    interface ExtUse { }
}
