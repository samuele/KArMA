package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.StringValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.StrType;

abstract class StringTernOp extends TernOp {

    StringTernOp(int line, int column, Expression e1, Expression e2, Expression e3) {
        super(line, column, e1, e2, e3);
    }

    @Override
    public StrType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (StrType) super.typeCheck(env);
    }

    @Override
    protected abstract StrType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException;

    @Override
    public abstract StringValue exec(DynamicEnv env);
}
