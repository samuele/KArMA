package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.BooleanValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.BoolType;

public class BooleanLiteral extends Literal {

    private final boolean value;

    public BooleanLiteral(int line, int column, boolean value) {
        super(line, column);
        this.value = value;
    }

    @Override
    public BoolType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (BoolType) super.typeCheck(env);
    }

    @Override
    protected BoolType typeCheckImplementation(StaticEnv env) {
        return BoolType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return String.valueOf(value);
    }

    @Override
    public String label() {
        return super.label() + ": " + value;
    }

    @Override
    public BooleanValue exec(DynamicEnv env) {
        return new BooleanValue(value);
    }
}
