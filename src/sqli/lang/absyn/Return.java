package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.SeqBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ConcreteValue;
import sqli.lang.semantical.InvalidReturnStatementException;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.UnitType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Return extends Command {

    private final Expression exp;

    public Return(int line, int column, Expression exp) {
        super(line, column);
        this.exp = exp;
    }

    @Override
    protected UnitType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType returnType = env.getReturnType();

        if (returnType == null)
            throw new InvalidReturnStatementException("Return " + this + " must only be placed inside functions");

        PrimitiveType expType = exp.typeCheck(env);

        if (returnType != expType)
            throw new TypeMismatchException("Return expression " + exp + " of " + this + " has type " + expType
                    + " instead of the required return type " + returnType);

        return UnitType.INSTANCE;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>(exp.use());
    }

    @Override
    protected Set<ExtUse> extendedUseImplementation() {
        return new HashSet<>(exp.extendedUse());
    }

    @Override
    protected Set<VariableName> defImplementation() {
        return new HashSet<>();
    }

    @Override
    protected Set<Return> retImplementation() {
        Set<Return> ret = new HashSet<>();
        ret.add(this);

        return ret;
    }

    @Override
    public String toCFGString() {
        return "return " + exp.toCFGString();
    }

    @Override
    public SeqBlock toCFGBlock() {
        return (SeqBlock) super.toCFGBlock();
    }

    @Override
    protected SeqBlock toCFGBlockImplementation() {
        return new SeqBlock(this);
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.linkNodeToNode(super.toDot(dot), exp.toDot(dot), "exp");
    }

    @Override
    public void removeSSA() {
        exp.removeSSA();
    }

    @Override
    public ConcreteValue<?> exec(DynamicEnv env) {
        return exp.exec(env);
    }
}
