package sqli.lang.absyn;

import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.semantical.TypeMismatchException;
import sqli.lang.types.BoolType;
import sqli.lang.types.PrimitiveType;

abstract class SameTypeComparisonBinOp extends ComparisonBinOp {

    SameTypeComparisonBinOp(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }

    @Override
    protected BoolType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType leftType = left.typeCheck(env);
        PrimitiveType rightType = right.typeCheck(env);

        if (leftType != rightType)
            throw new TypeMismatchException("Left operand " + left + " and right operand " + right + " of " + this
                    + " have different types or one of them is not a basic type");

        return BoolType.INSTANCE;
    }
}
