package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.FloatValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.FloatType;

public class ToFloat extends ArithmeticCastOp {

    public ToFloat(int line, int column, Expression exp) {
        super(line, column, exp);
    }

    @Override
    public FloatType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (FloatType) super.typeCheck(env);
    }

    @Override
    protected FloatType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        exp.typeCheck(env);
        return FloatType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "tofloat(" + exp.toCFGString() + ")";
    }

    @Override
    public FloatValue exec(DynamicEnv env) {
        return new FloatValue(Float.parseFloat(exp.exec(env).getValue().toString()));
    }
}
