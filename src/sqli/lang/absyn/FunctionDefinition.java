package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.SeqBlock;
import sqli.lang.semantical.MissingReturnException;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.FunctionType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class FunctionDefinition extends SubProgram implements Ret {

    private final FunctionDeclaration decl;
    private final VariableDefinition vars;
    private final Command body;
    private final FunctionDefinition next;

    private Set<Return> ret;


    public FunctionDefinition(int line, int column, FunctionDeclaration decl, VariableDefinition vars, Command body,
                              FunctionDefinition next) {
        super(line, column);
        this.decl = decl;
        this.vars = vars;
        this.body = body;
        this.next = next;
    }

    public FunctionDeclaration getDecl() {
        return decl;
    }

    Set<FunctionDefinition> collectFunctionDefinitions() {
        Set<FunctionDefinition> functionDefinitions = new HashSet<>();
        functionDefinitions.add(this);

        if (next != null)
            functionDefinitions.addAll(next.collectFunctionDefinitions());

        return functionDefinitions;
    }

    @Override
    public FunctionType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (FunctionType) super.typeCheck(env);
    }

    @Override
    protected FunctionType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        FunctionType funType = decl.typeCheck(env);
        envOwned = decl.getNewEnv();

        if (vars != null)
            vars.typeCheck(envOwned);

        body.typeCheck(envOwned);

        if (!hasReturn())
            throw new MissingReturnException("Function " + decl.getFun().getId() + " does not return on every path");

        if (next != null)
            next.typeCheck(env);

        return funType;
    }

    @Override
    public Set<Return> ret() {
        return ret != null ? ret : (ret = new HashSet<>(body.ret()));
    }

    @Override
    public boolean hasReturn() {
        return !ret().isEmpty();
    }

    @Override
    protected BasicBlock toCFGBlockImplementation() {
        BasicBlock comBlock = body.toCFGBlock();

        SeqBlock varsBlock = null;
        if (vars != null) {
            varsBlock = vars.toCFGBlock();
            varsBlock.completeWith(comBlock);
        }

        SeqBlock paramsBlock = null;
        if (decl.getParams() != null) {
            paramsBlock = decl.getParams().toCFGBlock();
            paramsBlock.completeWith(varsBlock != null ? varsBlock : comBlock);
        }

        return paramsBlock != null ? paramsBlock : (varsBlock != null ? varsBlock : comBlock);
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.linkNodeToNode(super.toDot(dot), decl.toDot(dot), "decl");

        if (vars != null)
            dot.linkNodeToNode(this, vars.toDot(dot), "var");

        if (next != null)
            dot.boldLinkNodeToNode(this, next.toDot(dot), "next");

        return dot.linkNodeToNode(this, body.toDot(dot), "body");
    }
}
