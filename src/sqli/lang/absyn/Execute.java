package sqli.lang.absyn;

import controller.ProcessHandler;
import dot.Dot;
import dot.DotableNode;
import javafx.scene.control.Alert;
import sqli.analysis.cfg.SeqBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.UnitValue;
import sqli.lang.semantical.ExecArgumentException;
import sqli.lang.semantical.ExecMisplacedException;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.PrimitiveType;
import sqli.lang.types.StrType;
import sqli.lang.types.UnitType;
import sqli.lang.types.VoidType;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Execute extends Command {

    private final Expression exp;

    public Execute(int line, int column, Expression exp) {
        super(line, column);
        this.exp = exp;
    }

    @Override
    public UnitType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        PrimitiveType expType = exp.typeCheck(env);

        if (expType != StrType.INSTANCE)
            throw new ExecArgumentException("Execute argument " + exp + " of " + this + " has type " + expType
                    + " instead of " + StrType.INSTANCE);

        if (env.getReturnType() != VoidType.INSTANCE)
            throw new ExecMisplacedException("Execute " + this + " must not be placed inside functions");

        return UnitType.INSTANCE;
    }

    @Override
    protected Set<VariableName> useImplementation() {
        return new HashSet<>(exp.use());
    }

    @Override
    protected Set<ExtUse> extendedUseImplementation() {
        return new HashSet<>(exp.extendedUse());
    }

    @Override
    protected Set<VariableName> defImplementation() {
        return new HashSet<>();
    }

    @Override
    protected Set<Return> retImplementation() {
        return new HashSet<>();
    }

    @Override
    public String toCFGString() {
        return "execute(" + exp.toCFGString() + ")";
    }

    @Override
    public SeqBlock toCFGBlock() {
        return (SeqBlock) super.toCFGBlock();
    }

    @Override
    protected SeqBlock toCFGBlockImplementation() {
        return new SeqBlock(this);
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.linkNodeToNode(super.toDot(dot), exp.toDot(dot), "exp");
    }

    @Override
    public void removeSSA() {
        exp.removeSSA();
    }

    @Override
    public UnitValue exec(DynamicEnv env) {
        String query = (String) exp.exec(env).getValue();
        ProcessHandler.getInstance().alert(query, "Query execution", "Execute", Alert.AlertType.INFORMATION);

        return UnitValue.INSTANCE;
    }
}
