package sqli.lang.absyn;

import dot.Dot;
import dot.DotableNode;
import sqli.analysis.cfg.BasicBlock;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.UnitType;

import java.io.IOException;
import java.util.Set;

public abstract class Command extends Absyn implements Typeable, Ret, Analyzable {

    private UnitType type;
    private Set<VariableName> use;
    private Set<ExtUse> extendedUse;
    private Set<VariableName> def;
    private Set<Return> ret;
    private BasicBlock block;

    Command(int line, int column) {
        super(line, column);
    }

    @Override
    public UnitType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return type != null ? type : (type = typeCheckImplementation(env));
    }

    protected abstract UnitType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException;

    @Override
    public Set<VariableName> use() {
        return use != null ? use : (use = useImplementation());
    }

    protected abstract Set<VariableName> useImplementation();

    @Override
    public Set<ExtUse> extendedUse() {
        return extendedUse != null ? extendedUse : (extendedUse = extendedUseImplementation());
    }

    protected abstract Set<ExtUse> extendedUseImplementation();

    @Override
    public Set<VariableName> def() {
        return def != null ? def : (def = defImplementation());
    }

    protected abstract Set<VariableName> defImplementation();

    @Override
    public Set<Return> ret() {
        return ret != null ? ret : (ret = retImplementation());
    }

    protected abstract Set<Return> retImplementation();

    @Override
    public boolean hasReturn() {
        return !ret().isEmpty();
    }

    @Override
    public BasicBlock toCFGBlock() {
        return block != null ? block : (block = toCFGBlockImplementation());
    }

    protected abstract BasicBlock toCFGBlockImplementation();

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return type != null
                ? dot.linkNodeToSymbol(super.toDot(dot), dot.dotSymbol(type.toString()), "type")
                : super.toDot(dot);
    }
}
