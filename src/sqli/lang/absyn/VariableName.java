package sqli.lang.absyn;

import static sqli.lang.absyn.ExtendedUse.ExtUse;

public class VariableName extends Name implements ExtUse, RemoveSSA {

    private String ssaName;

    public VariableName(int line, int column, String id) {
        super(line, column, id);
    }

    public String getSsaName() {
        return ssaName;
    }

    public void setSsaName(String ssaName) {
        this.ssaName = ssaName;
    }

    @Override
    public String toCFGString() {
        return ssaName != null ? ssaName : super.toCFGString();
    }

    @Override
    public void removeSSA() {
        ssaName = null;
    }
}
