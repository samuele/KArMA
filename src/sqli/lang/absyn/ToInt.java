package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.IntegerValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.IntType;

public class ToInt extends ArithmeticCastOp {

    public ToInt(int line, int column, Expression exp) {
        super(line, column, exp);
    }

    @Override
    public IntType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (IntType) super.typeCheck(env);
    }

    @Override
    protected IntType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        exp.typeCheck(env);
        return IntType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "toint(" + exp.toCFGString() + ")";
    }

    @Override
    public IntegerValue exec(DynamicEnv env) {
        return new IntegerValue(Integer.parseInt(exp.exec(env).getValue().toString()));
    }
}
