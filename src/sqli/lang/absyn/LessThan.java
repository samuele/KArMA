package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ArithmeticValue;
import sqli.analysis.monitor.value.BooleanValue;

public class LessThan extends ArithmeticComparisonBinOp {

    public LessThan(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }

    @Override
    public String toCFGString() {
        return "(" + left.toCFGString() + " < " + right.toCFGString() + ")";
    }

    @Override
    public BooleanValue exec(DynamicEnv env) {
        double d1 = ((ArithmeticValue<?>) left.exec(env)).getDoubleValue();
        double d2 = ((ArithmeticValue<?>) right.exec(env)).getDoubleValue();

        return new BooleanValue(Double.compare(d1, d2) < 0);
    }
}
