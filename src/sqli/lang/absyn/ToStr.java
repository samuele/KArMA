package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.StringValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.StrType;

public class ToStr extends CastOp {

    public ToStr(int line, int column, Expression exp) {
        super(line, column, exp);
    }

    @Override
    public StrType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (StrType) super.typeCheck(env);
    }

    @Override
    protected StrType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException {
        exp.typeCheck(env);
        return StrType.INSTANCE;
    }

    @Override
    public String toCFGString() {
        return "tostr(" + exp.toCFGString() + ")";
    }

    @Override
    public StringValue exec(DynamicEnv env) {
        return new StringValue(exp.exec(env).getValue().toString());
    }
}
