package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ArithmeticValue;
import sqli.analysis.monitor.value.FloatValue;
import sqli.analysis.monitor.value.IntegerValue;

public class Addition extends ArithmeticBinOp {

    public Addition(int line, int column, Expression left, Expression right) {
        super(line, column, left, right);
    }

    @Override
    public String toCFGString() {
        return "(" + left.toCFGString() + " + " + right.toCFGString() + ")";
    }

    @Override
    public ArithmeticValue<?> exec(DynamicEnv env) {
        ArithmeticValue<?> numValue = (ArithmeticValue<?>) left.exec(env);
        ArithmeticValue<?> denValue = (ArithmeticValue<?>) right.exec(env);

        if (numValue instanceof IntegerValue && denValue instanceof IntegerValue)
            return new IntegerValue((Integer) numValue.getValue() + (Integer) denValue.getValue());

        return new FloatValue((Float) numValue.getValue() + (Float) denValue.getValue());
    }
}
