package sqli.lang.absyn;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.ArithmeticValue;
import sqli.lang.semantical.SemanticalAnalyzerException;
import sqli.lang.semantical.StaticEnv;
import sqli.lang.types.ArithmeticType;

abstract class ArithmeticUntrustedOp extends UntrustedOp {

    ArithmeticUntrustedOp(int line, int column) {
        super(line, column);
    }

    @Override
    public ArithmeticType typeCheck(StaticEnv env) throws SemanticalAnalyzerException {
        return (ArithmeticType) super.typeCheck(env);
    }

    @Override
    protected abstract ArithmeticType typeCheckImplementation(StaticEnv env) throws SemanticalAnalyzerException;

    @Override
    public abstract ArithmeticValue<?> exec(DynamicEnv env);
}
