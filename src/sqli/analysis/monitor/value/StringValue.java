package sqli.analysis.monitor.value;

public class StringValue extends ConcreteValue<String> {

    public StringValue(String s) {
        super(s);
    }
}
