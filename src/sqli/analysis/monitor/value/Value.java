package sqli.analysis.monitor.value;

public abstract class Value<T> {

    private final T value;

    Value() {
        this(null);
    }

    Value(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
