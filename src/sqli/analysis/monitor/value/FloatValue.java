package sqli.analysis.monitor.value;

public class FloatValue extends ArithmeticValue<Float> {

    public FloatValue(Float value) {
        super(value);
    }
}
