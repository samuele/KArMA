package sqli.analysis.monitor.value;

public class BranchingValue extends Value<Boolean> {

    public BranchingValue(Boolean value) {
        super(value);
    }
}
