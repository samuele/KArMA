package sqli.analysis.monitor.value;

public abstract class ArithmeticValue<T extends Number> extends ConcreteValue<T> {

    ArithmeticValue(T value) {
        super(value);
    }

    public double getDoubleValue() {
        return getValue().doubleValue();
    }
}
