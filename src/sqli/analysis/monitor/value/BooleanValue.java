package sqli.analysis.monitor.value;

public class BooleanValue extends ConcreteValue<Boolean> {

    public BooleanValue(Boolean value) {
        super(value);
    }
}
