package sqli.analysis.monitor.value;

public class IntegerValue extends ArithmeticValue<Integer> {

    public IntegerValue(Integer value) {
        super(value);
    }
}
