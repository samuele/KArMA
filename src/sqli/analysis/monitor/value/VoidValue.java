package sqli.analysis.monitor.value;

public class VoidValue extends ConcreteValue<Object>  {

    public static final VoidValue INSTANCE = new VoidValue();

    @Override
    public String toString() {
        return null;
    }
}
