package sqli.analysis.monitor.value;

public abstract class ConcreteValue<T> extends Value<T> {

    ConcreteValue() {
        this(null);
    }

    ConcreteValue(T value) {
        super(value);
    }

    @Override
    public String toString() {
        return getValue().toString();
    }
}
