package sqli.analysis.monitor;

import sqli.analysis.monitor.value.ConcreteValue;

import java.util.HashMap;
import java.util.Map;

public class DynamicEnv {

    private final Map<String, ConcreteValue<?>> table;
    private final Map<String, ConcreteValue<?>> untrustedTable;

    public DynamicEnv() {
        table = new HashMap<>();
        untrustedTable = new HashMap<>();
    }

    public <T> void addEntry(String var, ConcreteValue<T> value) {
        table.put(var, value);
    }

    public <T> void addUntrustedValue(String var, ConcreteValue<T> value) {
        untrustedTable.put(var, value);
    }

    public ConcreteValue<?> getUntrustedValue(String var) {
        return untrustedTable.get(var);
    }

    public ConcreteValue<?> getValue(String var) {
        return table.get(var);
    }
}
