package sqli.analysis.monitor;

import controller.ProcessHandler;
import sqli.analysis.cfg.CFG;
import sqli.analysis.contracts.Path;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import static controller.ErrorsHandler.err;
import static utils.Utils.toMon;

public class Main {

    public static void runMonitor(CFG cfg, Set<Path> paths) {
        String monName = toMon(ProcessHandler.getInstance().getSource());
        FileWriter where;

        try {
            where = new FileWriter(monName);
        } catch (IOException e) {
            err("I/O error", e);
            return;
        }

        Monitor.init(cfg, paths, where);
        Monitor mon = new Monitor();
        mon.run();

        try {
            where.close();
        } catch (IOException e) {
            err("I/O error", e);
        }
    }

    public static void closeDueToError(FileWriter where) {
        //noinspection EmptyCatchBlock
        try {
            where.close();
        } catch (IOException e) { }
    }

}
