package sqli.analysis.monitor;

import controller.ProcessHandler;
import javafx.scene.control.Alert;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.BranchingBlock;
import sqli.analysis.cfg.CFG;
import sqli.analysis.cfg.SeqBlock;
import sqli.analysis.contracts.Contract;
import sqli.analysis.contracts.Path;
import sqli.analysis.contracts.ValidationContractException;
import sqli.analysis.monitor.value.*;
import sqli.analysis.taint.Taintable;
import sqli.lang.absyn.*;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

public class Monitor {

    private static FileWriter where;
    private static CFG cfg;
    private static Set<Path> paths;
    private static Set<Edge> dedge;
    private static BasicBlock prev;

    private final SubProgram prog;
    private final DynamicEnv env;

    static void init(CFG cfg, Set<Path> paths, FileWriter where) {
        Monitor.where = where;
        Monitor.cfg = cfg;

        Set<Path> copy = new HashSet<>();
        //noinspection Convert2streamapi
        for (Path path : paths)
            copy.add(new Path(path));

        for (Path path : copy) {
            Collections.reverse(path);

            List<Integer> toRemove = new ArrayList<>();
            int idx = 0;
            for (Taintable t : path) {
                if (t instanceof SeqBlock && ((SeqBlock) t).getAbsyn() instanceof VariableDeclaration) {
                    toRemove.add(idx);
                }
                else if (t instanceof Call) {
                    toRemove.add(idx);
                    toRemove.add(idx + 1);
                }
                ++idx;
            }

            int deleted = 0;
            for (int i : toRemove) {
                path.remove(i - deleted);
                ++deleted;
            }
        }

        Monitor.paths = copy;

        dedge = new HashSet<>();
    }

    Monitor() {
        prog = cfg.getMain();
        env = new DynamicEnv();
    }

    public Monitor(FunctionDeclaration callee, DynamicEnv env) {
        this.env = env;

        SubProgram prog = null;
        for (FunctionDefinition def : cfg.getMain().collectFunctionDefinitions()) {
            if (def.getDecl() == callee) {
                prog = def;
                break;
            }
        }

        this.prog = prog;
    }

    public ConcreteValue<?> run() {
        try {
            return exec(cfg.getRootOf(prog));
        } catch (ValidationContractException e) {
            ProcessHandler.getInstance().alert("Execution terminated", "Monitor", "Validation error",
                    Alert.AlertType.WARNING);

            return VoidValue.INSTANCE;
        } catch (RuntimeException e) {
            StringWriter stackTrace = new StringWriter();
            e.printStackTrace(new PrintWriter(stackTrace));
            ProcessHandler.getInstance().alert(stackTrace.toString(), "Runtime exception", e.getMessage(),
                    Alert.AlertType.ERROR);

            return VoidValue.INSTANCE;
        }
    }

    private void monitor(BasicBlock b) throws ValidationContractException {
        if (prev != null) dedge.add(new Edge(prev, b));
        prev = b;

        if (b.getAbsyn() instanceof Execute) {
            Set<Contract> toVerify = new HashSet<>();
            boolean next = false;
            for (Path c : paths) {
                if (c.get(c.size() - 1) != b)
                    continue;

                for (int i = 0; (i < c.size() - 1) && !next; ++i)
                    if (!reachability((BasicBlock) c.get(i), (BasicBlock) c.get(i + 1)))
                        next = true;

                if (next) {
                    next = false;
                    continue;
                }

                toVerify.add(c.getContract());
            }

            verify(toVerify);
        }
    }

    private ConcreteValue<?> exec(BasicBlock b) throws ValidationContractException {
        if (b == null)
            return VoidValue.INSTANCE;

        if (b.getAbsyn() instanceof VariableDeclaration)
            return exec(((SeqBlock) b).getSucc());

        monitor(b);

        Value<?> r = b.exec(env);

        if (r instanceof EndValue)
            return VoidValue.INSTANCE;

        if (r instanceof UnitValue)
            return exec(((SeqBlock) b).getSucc());

        if (r instanceof BranchingValue && (Boolean) r.getValue())
            return exec(((BranchingBlock) b).getYesSucc());

        if (r instanceof BranchingValue)
            return exec(((BranchingBlock) b).getNoSucc());

        return (ConcreteValue<?>) r;
    }

    private void verify(Set<Contract> toCheck) throws ValidationContractException {
        for (Contract c : toCheck) {
            if (!c.isValidationContractSatisfied(env, where))
                throw new ValidationContractException();
        }
    }

    private boolean reachability(BasicBlock from, BasicBlock to) {
        List<BasicBlock> l = new ArrayList<>();
        l.add(from);

        Set<BasicBlock> seen = new HashSet<>();
        while (!l.isEmpty()) {
            BasicBlock first = l.get(0);

            if (seen.contains(first)) {
                l.remove(0);
                continue;
            }

            for (Edge e : dedge) {
                if (e.getFrom().equals(first)) {
                    if (e.getTo().equals(to)) return true;
                    l.add(e.getTo());
                }
            }

            seen.add(l.remove(0));
        }

        return false;
    }

    private static class Edge {

        private BasicBlock from;
        private BasicBlock to;

        private Edge(BasicBlock from, BasicBlock to) {
            this.from = from;
            this.to = to;
        }

        private BasicBlock getFrom() {
            return from;
        }

        private BasicBlock getTo() {
            return to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Edge edge = (Edge) o;

            if (!from.equals(edge.from)) return false;
            return to.equals(edge.to);
        }

        @Override
        public int hashCode() {
            int result = from.hashCode();
            result = 31 * result + to.hashCode();
            return result;
        }
    }
}
