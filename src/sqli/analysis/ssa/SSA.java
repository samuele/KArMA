package sqli.analysis.ssa;

import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.CFG;
import sqli.lang.absyn.Analyzable;
import sqli.lang.absyn.SubProgram;
import sqli.lang.absyn.VariableName;
import sqli.lang.semantical.StaticEnv;

import java.util.*;

class SSA {

    private final CFG cfg;
    private final HashMap<SubProgram, Set<String>> globals;

    public SSA(CFG cfg) {
        this.cfg = cfg;

        globals = new HashMap<>();
        for (SubProgram prog : cfg.getProgs())
            globals.put(prog, new HashSet<>());
    }

    public void run() {
        for (SubProgram prog : cfg.getProgs())
            computeVariablesRenaming(prog, computePhiFunctions(prog, computeGlobalNames(prog)));
    }

    private Map<String, Set<BasicBlock>> computeGlobalNames(SubProgram prog) {
        Map<String, Set<BasicBlock>> blocks = new HashMap<>();

        StaticEnv env = prog.getEnvOwned();

        for (String var : env.getVarsKeySet())
            blocks.put(var, new HashSet<>());

        for (BasicBlock block : cfg.getBlocksOf(prog)) {
            Analyzable com = block.getAbsyn();
            Set<VariableName> uses = com.use();
            Set<VariableName> definitions = com.def();

            for (VariableName var : uses)
                globals.get(prog).add(var.getId());

            for (VariableName var : definitions)
                blocks.get(var.getId()).add(block);
        }

        return blocks;
    }

    private Map<String, Set<BasicBlock>> computePhiFunctions(SubProgram prog, Map<String, Set<BasicBlock>> blocks) {
        for (String var : globals.get(prog)) {
            List<BasicBlock> workList = new ArrayList<>(blocks.get(var));

            for (int i = 0; i < workList.size(); ++i) {
                BasicBlock b = workList.get(i);

                //noinspection Convert2streamapi
                for (BasicBlock d : b.getDominanceFrontier()) {
                    if (!(d.containsPhiFunction(var))) {
                        d.addPhiFunction(var);
                        workList.add(d);
                    }
                }
            }
        }

        return blocks;
    }

    private void computeVariablesRenaming(SubProgram prog, Map<String, Set<BasicBlock>> blocks) {
        Map<String, Integer> varToCounter = new HashMap<>();
        Map<String, Stack<Integer>> varToStack = new HashMap<>();

        for (String var : globals.get(prog)) {
            varToStack.put(var, new Stack<>());
            varToCounter.put(var, 0);
        }

        for (String var : blocks.keySet()) {
            if (varToStack.containsKey(var))
                continue;
            varToStack.put(var, new Stack<>());
            varToCounter.put(var, 0);
        }

        rename(prog, cfg.getRootOf(prog), varToStack, varToCounter);
    }

    private void rename(SubProgram prog, BasicBlock b, Map<String, Stack<Integer>> varToStack, Map<String,
            Integer> varToCounter) {
        for (PhiFunction phi : b.getPhiFunctions().values()) {
            phi.setSsaName(newName(phi.getVar(), varToStack, varToCounter));
            cfg.addReachingDefinitionFor(prog, phi.getSsaName(), b);
        }

        for (VariableName var : b.getAbsyn().use())
            var.setSsaName(var.getId() + "_" + varToStack.get(var.getId()).peek());

        Set<VariableName> definitions = b.getAbsyn().def();
        for (VariableName var : definitions) {
            var.setSsaName(newName(var.getId(), varToStack, varToCounter));
            cfg.addReachingDefinitionFor(prog, var.getSsaName(), b);
        }

        for (BasicBlock succ : b.getSuccessors())
            for (PhiFunction phi : succ.getPhiFunctions().values())
                phi.addParam(phi.getVar() + "_" + varToStack.get(phi.getVar()).peek());

        for (BasicBlock succ : b.getDomTreeSuccessors())
            rename(prog, succ, varToStack, varToCounter);

        for (PhiFunction phi : b.getPhiFunctions().values())
            varToStack.get(phi.getVar()).pop();

        for (VariableName var : definitions)
            varToStack.get(var.getId()).pop();
    }

    private String newName(String var, Map<String, Stack<Integer>> varToStack, Map<String, Integer> varToCounter) {
        int i = varToCounter.get(var);
        varToCounter.put(var, i + 1);
        varToStack.get(var).push(i);

        return var + "_" + i;
    }
}
