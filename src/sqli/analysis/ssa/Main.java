package sqli.analysis.ssa;

import dot.Dot;
import sqli.analysis.cfg.CFG;
import sqli.analysis.cfg.View;
import sqli.lang.absyn.SubProgram;

import java.io.IOException;

import static controller.ErrorsHandler.err;
import static controller.ErrorsHandler.info;

public class Main {

    public static void runSsaConstruction(CFG cfg) {
        SSA ssa = new SSA(cfg);
        ssa.run();

        View view = new View(cfg);
        try (Dot dot = new Dot("output/" + cfg.getMain().getName().getId() + "_ssa.dot")) {
            for (SubProgram prog : cfg.getProgs())
                view.printCFG(prog, dot);
        } catch (IOException e) {
            err("I/O error", e);
        }

        info("End of the SSA form construction");
        info("SSA form saved into output/" + cfg.getMain().getName().getId() + "_ssa.dot");
    }
}
