package sqli.analysis.ssa;

import sqli.lang.absyn.CFGPrintable;
import sqli.lang.absyn.ExtendedUse;
import sqli.lang.absyn.Use;
import sqli.lang.absyn.VariableName;

import java.util.HashSet;
import java.util.Set;

public class PhiFunction implements Use, ExtendedUse, CFGPrintable {

    private static int counter = 0;
    private final int identifier;

    private final String var;
    private final Set<String> params;

    private Set<VariableName> use;
    private Set<ExtUse> extendedUse;

    private String ssaName;

    public PhiFunction(String var) {
        this.var = var;
        params = new HashSet<>();
        identifier = counter++;
    }

    String getVar() {
        return var;
    }

    String getSsaName() {
        return ssaName;
    }

    void setSsaName(String ssaName) {
        this.ssaName = ssaName;
    }

    void addParam(String param) {
        params.add(param);
    }

    @Override
    public Set<VariableName> use() {
        if (use != null)
            return use;

        Set<VariableName> use = new HashSet<>();
        for (String param : params) {
            VariableName v = new VariableName(-identifier, -identifier, param);
            v.setSsaName(param);
            use.add(v);
        }

        return this.use = use;
    }

    @Override
    public Set<ExtUse> extendedUse() {
        return extendedUse != null ? extendedUse : (extendedUse = new HashSet<>(use()));
    }

    @Override
    public String toCFGString() {
        String exp = ssaName + " := phi(";
        for (String param : params)
            exp += param + ", ";

        return exp.substring(0, exp.length() - 2) + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhiFunction that = (PhiFunction) o;

        return var.equals(that.var);
    }

    @Override
    public int hashCode() {
        return var.hashCode();
    }

    @Override
    public void removeSSA() {
        throw new UnsupportedOperationException();
    }
}
