package sqli.analysis.contracts;

import dot.Dot;
import dot.Dotable;
import dot.DotableNode;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.monitor.DynamicEnv;
import sqli.lang.absyn.VariableDefinition;

import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static controller.ErrorsHandler.err;

public class Contract implements Dotable {

    private final Path path;
    private final ValidationContract v;

    public Contract(Path path, ValidationContract v) {
        this.path = path;
        this.v = v;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.redDashedLinkNodeToNode(v.toDot(dot), path.toDot(dot), "");
    }

    public boolean isValidationContractSatisfied(DynamicEnv env, FileWriter where) {
        String var = ((VariableDefinition) ((BasicBlock) path.last()).getAbsyn()).getDecl().getVar().getId();
        String untrusted = env.getUntrustedValue(var).toString();

        Pattern regex = v.toRegex();
        Matcher m = regex.matcher(untrusted);
        boolean res = m.matches();

        try {
            where.write("Checking if " + untrusted + " satisfy " + regex.toString() + "... " + res + "\n");
        } catch (IOException e) {
            sqli.analysis.monitor.Main.closeDueToError(where);
            err("I/O error", e);
            return res;
        }

        return res;
    }
}
