package sqli.analysis.contracts;

import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.taint.Taintable;
import sqli.analysis.taint.Tree;
import sqli.lang.absyn.UntrustedOp;
import sqli.lang.absyn.VariableDefinition;

import java.util.HashSet;
import java.util.Set;

class PathBuilder {

    private final Set<Tree<BasicBlock>> trees;

    PathBuilder(Set<Tree<BasicBlock>> trees) {
        this.trees = trees;
    }

    Set<Path> run() {
        Set<Path> paths = new HashSet<>();

        for (Tree<BasicBlock> tree : trees)
            dfs(paths, new Path(), tree);

        return paths;
    }

    private void dfs(Set<Path> paths, Path path, Tree<? extends Taintable> tree) {
        path.add(tree.getNode());

        if (tree.isLeaf()) {
            if (tree.getNode() instanceof BasicBlock) {
                BasicBlock b = (BasicBlock) tree.getNode();
                if (b.getAbsyn() instanceof VariableDefinition) {
                    VariableDefinition def = (VariableDefinition) b.getAbsyn();
                    if (def.getExp() instanceof UntrustedOp) {
                        path.setIdentifier();
                        paths.add(path);
                    }
                }
            }

            return;
        }

        for (Tree<? extends Taintable> child : tree.getChildren())
            dfs(paths, new Path(path), child);
    }
}
