package sqli.analysis.contracts;

import controller.ProcessHandler;
import dot.Dot;
import dot.DotableNode;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;

import java.io.IOException;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class ValidationContract implements DotableNode {

    private static final ProcessHandler pHandler = ProcessHandler.getInstance();

    private final String v;

    private String nodeName;

    private ValidationContract(String v) {
        this.v = v;
    }

    public static ValidationContract build() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Validation contract input");
        dialog.setHeaderText("Insert a new validation contract");
        dialog.setContentText("Validation contract:");

        Optional<String> result = dialog.showAndWait();
        if (!result.isPresent()) {
            pHandler.alert("Validation contract error", "Invalid validation contract", "Please insert a valid regex",
                    Alert.AlertType.ERROR );
            return build();
        }
        else if (!testRegex(result.get())) {
            return build();
        }

        return new ValidationContract(result.get());
    }

    private static boolean testRegex(String s) {
        try {
            //noinspection ResultOfMethodCallIgnored
            Pattern.compile(s);
        } catch (PatternSyntaxException exception) {
            pHandler.alert(exception.getLocalizedMessage(), "Validation contract error", "Invalid validation contract",
                    Alert.AlertType.ERROR );
            return false;
        }

        return true;
    }

    Pattern toRegex() {
        return Pattern.compile(v);
    }

    @Override
    public String getNodeName() {
        return nodeName;
    }

    @Override
    public String setNodeName(String nodeName) {
        return this.nodeName = nodeName;
    }

    @Override
    public String label() {
        return v;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.dotRedBoxNode(this);
    }
}
