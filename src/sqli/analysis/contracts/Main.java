package sqli.analysis.contracts;

import controller.ProcessHandler;
import dot.Dot;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.taint.Tree;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static controller.ErrorsHandler.err;
import static controller.ErrorsHandler.info;
import static utils.Utils.asSortedList;

public class Main {

    public static Set<Path> runContractsRequest(Set<Tree<BasicBlock>> trees, String name) {
        Set<Path> paths = new PathBuilder(trees).run();
        Set<Contract> contracts = new HashSet<>();

        //noinspection Convert2streamapi
        for (Path path : asSortedList(paths)) {
            try (Dot dot = new Dot("output/" + name + "_path" + path.getIdentifier() + ".dot")) {
                path.toDot(dot);
            } catch (IOException e) {
                err("I/O error", e);
            }

            ProcessHandler.getInstance().spawnAskForContractStage(path);
            contracts.add(path.getContract());
        }

        try (Dot dot = new Dot("output/" + name + "_contracts.dot")) {
            for (Contract contract : contracts)
                contract.toDot(dot);
        } catch (IOException e) {
            err("I/O error", e);
        }

        info("End of the contracts request");
        info("Contracts saved into output/" + name + "_contracts.dot");

        return paths;
    }
}
