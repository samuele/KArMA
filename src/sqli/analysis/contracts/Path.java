package sqli.analysis.contracts;

import dot.Dot;
import dot.Dotable;
import dot.DotableNode;
import dot.SimpleNode;
import sqli.analysis.taint.Taintable;
import sqli.lang.absyn.Call;

import java.io.IOException;
import java.util.ArrayList;

public class Path extends ArrayList<Taintable> implements Dotable, Comparable<Path> {

    private static int counter = 1;

    private Contract contract;
    private int identifier;

    Path() { }

    public Path(Path path) {
        super(path);
        contract = path.contract;
        identifier = path.identifier;
    }

    public static int getCounter() {
        return counter;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public int getIdentifier() {
        return identifier;
    }

    void setIdentifier() {
        identifier = counter++;
    }

    public Taintable head() {
        return get(0);
    }

    public void removeHead() {
        remove(0);
    }

    Taintable last() {
        return get(size() - 1);
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        DotableNode last = null;
        DotableNode toRet = null;

        for (Taintable t : this) {
            DotableNode next = !t.isInstanceOfCall() ? t.toDot(dot) : (new SimpleNode() {

                @Override
                public String label() {
                    return ((Call) t).toCFGString();
                }

            }).toDot(dot);

            if (last != null)
                dot.linkNodeToNode(last, next, "");
            else
                toRet = next;

            last = next;
        }

        return toRet;
    }

    @Override
    public int compareTo(Path o) {
        return Integer.compare(identifier, o.identifier);
    }
}
