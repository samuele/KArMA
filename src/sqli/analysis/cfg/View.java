package sqli.analysis.cfg;

import dot.Dot;
import sqli.lang.absyn.SubProgram;

import java.io.IOException;
import java.util.Set;

import static controller.ErrorsHandler.info;

public class View {

    private final CFG cfg;

    public View(CFG cfg) {
        this.cfg = cfg;
    }

    void printAll() throws IOException {
        Dot dot;
        String name = cfg.getMain().getName().getId();

        dot = new Dot("output/" + name + "_cfg.dot");
        for (SubProgram prog : cfg.getProgs())
            printCFG(prog, dot);
        info("Control flow graph saved into " + dot.getFilename());
        dot.close();

        dot = new Dot("output/" + name + "_doms.dot");
        for (SubProgram prog : cfg.getProgs())
            printDominators(prog, dot);
        info("Dominators graph saved into " + dot.getFilename());
        dot.close();

        dot = new Dot("output/" + name + "_frontiers.dot");
        for (SubProgram prog : cfg.getProgs())
            printDominanceFrontiers(prog, dot);
        info("Dominance frontiers graph saved into " + dot.getFilename());
        dot.close();

        dot = new Dot("output/" + name + "_domtree.dot");
        for (SubProgram prog : cfg.getProgs())
            printDominatorTree(prog, dot);
        info("Dominator tree saved into " + dot.getFilename());
        dot.close();
    }

    public void printCFG(SubProgram prog, Dot dot) throws IOException {
        Set<BasicBlock> blocks = cfg.getBlocksOf(prog);
        BasicBlock root = cfg.getRootOf(prog);

        for (BasicBlock block : blocks)
            block.toDot(dot);

        String startSymbol = dot.dotDoubleCircleSymbol("start");
        String endSymbol = dot.dotDoubleCircleSymbol("end");

        dot.linkSymbolToNode(startSymbol, root, "");

        for (BasicBlock block : blocks) {
            for (BasicBlock succ : block.getSuccessors()) {
                String label = "";

                if (block.isBranchingBlock() && succ == ((BranchingBlock) block).getYesSucc())
                    label = "yes";
                else if (block.isBranchingBlock() && succ == ((BranchingBlock) block).getNoSucc())
                    label = "no";

                dot.linkNodeToNode(block, succ, label);
            }

            if (block.getSuccessors().isEmpty())
                dot.linkNodeToSymbol(block, endSymbol, "");
        }
    }

    private void printDominators(SubProgram prog, Dot dot) throws IOException {
        Set<BasicBlock> blocks = cfg.getBlocksOf(prog);

        printCFG(prog, dot);

        for (BasicBlock block : blocks)
            for (BasicBlock dominator : block.getDominators()) {
                if (dominator == block.getImmediateDominator())
                    dot.redLinkNodeToNode(dominator, block, "");
                else
                    dot.redDashedLinkNodeToNode(dominator, block, "");
            }
    }

    private void printDominanceFrontiers(SubProgram prog, Dot dot) throws IOException {
        Set<BasicBlock> blocks = cfg.getBlocksOf(prog);

        printCFG(prog, dot);

        for (BasicBlock block : blocks)
            for (BasicBlock f : block.getDominanceFrontier())
                dot.redLinkNodeToNode(block, f, "");
    }

    private void printDominatorTree(SubProgram prog, Dot dot) throws IOException {
        Set<BasicBlock> blocks = cfg.getBlocksOf(prog);

        printCFG(prog, dot);

        for (BasicBlock block : blocks) {
            if (block.getImmediateDominator() != null)
                dot.redLinkNodeToNode(block.getImmediateDominator(), block, "");
        }
    }
}
