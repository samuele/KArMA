package sqli.analysis.cfg;

import sqli.analysis.monitor.DynamicEnv;
import sqli.analysis.monitor.value.Value;
import sqli.analysis.ssa.PhiFunction;
import sqli.analysis.taint.Taintable;
import sqli.lang.absyn.Analyzable;
import sqli.lang.absyn.Call;
import sqli.lang.absyn.End;
import sqli.lang.absyn.Exec;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class BasicBlock implements Taintable, Exec {

    final Analyzable absyn;

    final Set<BasicBlock> successors;
    final Set<BasicBlock> predecessors;
    private final Set<BasicBlock> dominanceFrontier;
    private final Set<BasicBlock> domTreeSuccessors;
    private final Map<String, PhiFunction> phiFunctions;
    private Set<BasicBlock> dominators;
    private BasicBlock immediateDominator;

    private String nodeName;

    BasicBlock(Analyzable absyn) {
        this.absyn = absyn;
        successors = new HashSet<>();
        predecessors = new HashSet<>();
        dominators = new HashSet<>();
        dominanceFrontier = new HashSet<>();
        domTreeSuccessors = new HashSet<>();
        phiFunctions = new HashMap<>();
    }

    public Analyzable getAbsyn() {
        return absyn;
    }

    public Set<BasicBlock> getSuccessors() {
        return successors;
    }

    Set<BasicBlock> getPredecessors() {
        return predecessors;
    }

    Set<BasicBlock> getDominators() {
        return dominators;
    }

    void setDominators(Set<BasicBlock> dominators) {
        this.dominators = dominators;
    }

    void addDominator(BasicBlock dominator) {
        this.dominators.add(dominator);
    }

    void addDominators(Set<BasicBlock> dominators) {
        this.dominators.addAll(dominators);
    }

    BasicBlock getImmediateDominator() {
        return immediateDominator;
    }

    void setImmediateDominator(BasicBlock immediateDominator) {
        this.immediateDominator = immediateDominator;
    }

    public Set<BasicBlock> getDominanceFrontier() {
        return dominanceFrontier;
    }

    void addToDominanceFrontier(BasicBlock block) {
        dominanceFrontier.add(block);
    }

    public Set<BasicBlock> getDomTreeSuccessors() {
        return domTreeSuccessors;
    }

    void addDomTreeSuccessor(BasicBlock block) {
        domTreeSuccessors.add(block);
    }

    public Map<String, PhiFunction> getPhiFunctions() {
        return phiFunctions;
    }

    public void addPhiFunction(String var) {
        phiFunctions.put(var, new PhiFunction(var));
    }

    public boolean containsPhiFunction(String var) {
        return phiFunctions.containsKey(var);
    }

    @Override
    public boolean isInstanceOfCall() {
        return absyn instanceof Call;
    }

    @Override
    public String getNodeName() {
        return nodeName;
    }

    @Override
    public String setNodeName(String nodeName) {
        return this.nodeName = nodeName;
    }

    public abstract boolean isBranchingBlock();

    abstract Set<BasicBlock> nullSet();

    public void completeWith(BasicBlock succ) {
        for (BasicBlock block : nullSet()) {
            if (block.isBranchingBlock())
                ((BranchingBlock) block).setNoSucc(succ);
            else if (!(block.getAbsyn() instanceof End))
                ((SeqBlock) block).setSucc(succ);
        }
    }

    @Override
    public String label() {
        String phiFunctions = phiFunctionsToCfg();
        String code = absyn.toCFGString();

        return phiFunctions + code;
    }

    private String phiFunctionsToCfg() {
        String res = "";
        for (PhiFunction phiFunction : phiFunctions.values())
            res += phiFunction.toCFGString() + "\n";

        return res;
    }

    public void removeSSA() {
        phiFunctions.clear();
        absyn.removeSSA();
    }

    @Override
    public Value<?> exec(DynamicEnv env) {
        return absyn.exec(env);
    }
}
