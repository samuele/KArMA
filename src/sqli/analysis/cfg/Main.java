package sqli.analysis.cfg;

import sqli.lang.absyn.ProgDefinition;

import java.io.IOException;

import static controller.ErrorsHandler.err;
import static controller.ErrorsHandler.info;

public class Main {

    public static CFG runCfgConstruction(ProgDefinition absyn) {
        CFG cfg = new CFG(absyn);
        cfg.run();

        View view = new View(cfg);
        try {
            view.printAll();
        } catch (IOException e) {
            err("I/O error", e);
            return null;
        }

        info("End of the control flow graph construction");

        return cfg;
    }
}
