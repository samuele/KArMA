package sqli.analysis.cfg;

import sqli.lang.absyn.FunctionDefinition;
import sqli.lang.absyn.ProgDefinition;
import sqli.lang.absyn.SubProgram;

import java.util.*;

public class CFG {

    private final ProgDefinition main;
    private final Set<SubProgram> progs;
    private final HashMap<SubProgram, Set<BasicBlock>> progToBlocks;
    private final HashMap<SubProgram, HashMap<String, BasicBlock>> progToReachingDefinitions;
    private final HashMap<SubProgram, BasicBlock> progToRoot;


    public CFG(ProgDefinition main) {
        this.main = main;

        progs = new HashSet<>();
        progs.add(main);
        //noinspection Convert2streamapi
        for (FunctionDefinition f : main.collectFunctionDefinitions())
            progs.add(f);

        progToBlocks = new HashMap<>();
        progToReachingDefinitions = new HashMap<>();
        progToRoot = new HashMap<>();

        for (SubProgram prog : progs) {
            progToBlocks.put(prog, new HashSet<>());
            progToReachingDefinitions.put(prog, new HashMap<>());
        }
    }

    public ProgDefinition getMain() {
        return main;
    }

    public Set<SubProgram> getProgs() {
        return progs;
    }

    public Set<BasicBlock> getBlocksOf(SubProgram prog) {
        return progToBlocks.get(prog);
    }

    public void addReachingDefinitionFor(SubProgram prog, String var, BasicBlock b) {
        progToReachingDefinitions.get(prog).put(var, b);
    }

    public BasicBlock reachingDefinitions(SubProgram prog, String var) {
        return progToReachingDefinitions.get(prog).get(var);
    }

    public BasicBlock getRootOf(SubProgram prog) {
        return progToRoot.get(prog);
    }

    public void run() {
        for (SubProgram prog : progs) {
            computeCFG(prog);
            computeAllDominators(prog);
            computeAllImmediateDominators(prog);
            computeAllDominanceFrontiers(prog);
            computeDominatorsTree(prog);
        }
    }

    private void computeCFG(SubProgram prog) {
        Set<BasicBlock> blocks = progToBlocks.get(prog);

        List<BasicBlock> q = new ArrayList<>();
        q.add(prog.toCFGBlock());

        BasicBlock root = null;

        while (!q.isEmpty()) {
            BasicBlock block = q.remove(0);

            if (blocks.contains(block))
                continue;

            blocks.add(block);
            q.addAll(block.getSuccessors());

            if (root == null && block.getPredecessors().isEmpty())
                root = block;
        }

        progToRoot.put(prog, root);
    }

    private void computeAllDominators(SubProgram prog) {
        BasicBlock root = progToRoot.get(prog);
        Set<BasicBlock> blocks = progToBlocks.get(prog);

        root.addDominator(root);

        //noinspection Convert2streamapi
        for (BasicBlock block : blocks)
            if (block != root)
                block.addDominators(blocks);

        boolean changed = true;

        while (changed) {
            changed = false;

            for (BasicBlock block : blocks) {
                Set<BasicBlock> temp = new HashSet<>();
                temp.add(block);

                Set<BasicBlock> intersection = null;
                for (BasicBlock pred : block.getPredecessors()) {
                    if (intersection == null)
                        intersection = new HashSet<>(pred.getDominators());
                    else
                        intersection.retainAll(pred.getDominators());
                }

                if (intersection != null)
                    temp.addAll(intersection);

                if (!temp.equals(block.getDominators())) {
                    block.setDominators(temp);
                    changed = true;
                }
            }
        }
    }

    private void computeAllImmediateDominators(SubProgram prog) {
        BasicBlock root = progToRoot.get(prog);
        Set<BasicBlock> blocks = progToBlocks.get(prog);

        for (BasicBlock block : blocks) {
            if (block == root)
                continue;

            int min = Integer.MAX_VALUE;
            BasicBlock candidate = null;

            for (BasicBlock dominator : block.getDominators()) {
                if (block == dominator)
                    continue;

                int temp;
                if ((temp = distanceToStrictDominator(block, dominator)) < min) {
                    candidate = dominator;
                    min = temp;

                    if (min == 1)
                        break;
                }
            }

            block.setImmediateDominator(candidate);
        }
    }

    private int distanceToStrictDominator(BasicBlock block, BasicBlock dominator) {
        int dist = 1;
        BasicBlock sep = block;

        List<BasicBlock> q = new ArrayList<>();
        q.add(block);

        while (!q.isEmpty()) {
            BasicBlock temp = q.remove(0);

            if (temp.getPredecessors().contains(dominator))
                break;

            q.addAll(temp.getPredecessors());

            if (sep == temp) {
                ++dist;
                sep = q.get(q.size() - 1);
            }
        }

        return dist;
    }

    private void computeAllDominanceFrontiers(SubProgram prog) {
        Set<BasicBlock> blocks = progToBlocks.get(prog);

        for (BasicBlock block : blocks) {
            if (block.getPredecessors().size() > 1) {
                for (BasicBlock pred : block.getPredecessors()) {
                    BasicBlock runner = pred;
                    while (runner != block.getImmediateDominator()) {
                        runner.addToDominanceFrontier(block);
                        runner = runner.getImmediateDominator();
                    }
                }
            }
        }
    }

    private void computeDominatorsTree(SubProgram prog) {
        Set<BasicBlock> blocks = progToBlocks.get(prog);

        //noinspection Convert2streamapi
        for (BasicBlock block : blocks)
            if (block.getImmediateDominator() != null)
                block.getImmediateDominator().addDomTreeSuccessor(block);
    }

    public void removeSSA() {
        for (SubProgram prog : progs)
            //noinspection Convert2streamapi
            for (BasicBlock block : getBlocksOf(prog))
                block.removeSSA();
    }
}
