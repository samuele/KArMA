package sqli.analysis.cfg;

import dot.Dot;
import dot.DotableNode;
import sqli.lang.absyn.Analyzable;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class SeqBlock extends BasicBlock {

    private BasicBlock succ;

    public SeqBlock(Analyzable absyn) {
        super(absyn);
    }

    public BasicBlock getSucc() {
        return succ;
    }

    public void setSucc(BasicBlock succ) {
        this.succ = succ;

        successors.add(succ);
        succ.predecessors.add(this);
    }

    @Override
    public boolean isBranchingBlock() {
        return false;
    }

    @Override
    public Set<BasicBlock> nullSet() {
        Set<BasicBlock> nullSet = new HashSet<>();

        if (succ == null)
            nullSet.add(this);
        else
            nullSet.addAll(succ.nullSet());

        return nullSet;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.dotBoxNode(this);
    }
}
