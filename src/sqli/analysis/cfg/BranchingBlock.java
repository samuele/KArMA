package sqli.analysis.cfg;

import dot.Dot;
import dot.DotableNode;
import sqli.lang.absyn.Analyzable;
import sqli.lang.absyn.While;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class BranchingBlock extends BasicBlock {

    private BasicBlock yesSucc;
    private BasicBlock noSucc;

    public BranchingBlock(Analyzable absyn) {
        super(absyn);
    }

    public BasicBlock getYesSucc() {
        return yesSucc;
    }

    public void setYesSucc(BasicBlock yesSucc) {
        this.yesSucc = yesSucc;

        successors.add(yesSucc);
        yesSucc.predecessors.add(this);
    }

    public BasicBlock getNoSucc() {
        return noSucc;
    }

    public void setNoSucc(BasicBlock noSucc) {
        this.noSucc = noSucc;

        successors.add(noSucc);
        noSucc.predecessors.add(this);
    }

    @Override
    public boolean isBranchingBlock() {
        return true;
    }

    @Override
    public Set<BasicBlock> nullSet() {
        Set<BasicBlock> nullSet = new HashSet<>();

        // yesSucc should not be null and the absyn associated to a
        // a branching node is either a While instance or an If
        // instance.

        boolean whileInstance = absyn instanceof While;

        if (noSucc == null && whileInstance)
            nullSet.add(this);
        else if (noSucc == null) {
            nullSet.add(this);
            nullSet.addAll(yesSucc.nullSet());
        } else if (whileInstance)
            nullSet.addAll(noSucc.nullSet());

        else {
            nullSet.addAll(yesSucc.nullSet());
            nullSet.addAll(noSucc.nullSet());
        }

        return nullSet;
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.dotHexagonNode(this);
    }
}
