package sqli.analysis.taint;

import dot.DotableNode;

public interface Taintable extends DotableNode {

    boolean isInstanceOfCall();
}
