package sqli.analysis.taint;

import java.util.ArrayList;

class CallHistory extends ArrayList<BlockCall> {

    CallHistory() { }

    CallHistory(CallHistory c) {
        //noinspection Convert2streamapi
        for (BlockCall blockCall : c)
            this.add(blockCall);
    }

    BlockCall last() {
        return get(size() - 1);
    }

    boolean isLastDuplicate() {
        String lastId = last().getCall().getFun().getId();
        for (BlockCall blockCall : this)
            if (blockCall != last() && blockCall.getCall().getFun().getId().equals(lastId))
                return true;

        return false;
    }
}
