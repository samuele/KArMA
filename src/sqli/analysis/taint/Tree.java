package sqli.analysis.taint;

import dot.Dot;
import dot.DotableNode;
import sqli.lang.absyn.Call;

import java.io.IOException;
import java.util.Set;

public class Tree<T extends Taintable> implements DotableNode {

    private final T node;
    private final Set<Tree<? extends Taintable>> children;

    private String nodeName;

    Tree(T node, Set<Tree<? extends Taintable>> children) {
        this.node = node;
        this.children = children;
    }

    public T getNode() {
        return node;
    }

    public Set<Tree<? extends Taintable>> getChildren() {
        return children;
    }

    @Override
    public String getNodeName() {
        return nodeName;
    }

    @Override
    public String setNodeName(String nodeName) {
        return this.nodeName = nodeName;
    }

    public boolean isLeaf() {
        return children.isEmpty();
    }

    @Override
    public String label() {
        return node.isInstanceOfCall() ? ((Call) node).toCFGString() : node.label();
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        dot.dotBoxNode(this);
        for (Tree<?> child : children)
            dot.linkNodeToNode(this, child.toDot(dot), "");

        return this;
    }
}
