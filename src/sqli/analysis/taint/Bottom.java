package sqli.analysis.taint;

import dot.Dot;
import dot.DotableNode;

import java.io.IOException;

class Bottom implements Taintable {

    private final int k;

    private String nodeName;

    public Bottom(int k) {
        this.k = k;
    }

    int getK() {
        return k;
    }

    @Override
    public boolean isInstanceOfCall() {
        return false;
    }

    @Override
    public String getNodeName() {
        return nodeName;
    }

    @Override
    public String setNodeName(String nodeName) {
        return this.nodeName = nodeName;
    }

    @Override
    public String label() {
        return "Bottom(" + k + ")";
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.dotBoxNode(this);
    }
}
