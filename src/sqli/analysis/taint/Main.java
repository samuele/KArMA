package sqli.analysis.taint;

import dot.Dot;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.CFG;

import java.io.IOException;
import java.util.Set;

import static controller.ErrorsHandler.err;
import static controller.ErrorsHandler.info;

public class Main {

    public static Set<Tree<BasicBlock>> runTaintAnalysis(CFG cfg) {
        Analyzer analyzer = new Analyzer(cfg);
        Set<Tree<BasicBlock>> trees = analyzer.run();

        try (Dot dot = new Dot("output/" + cfg.getMain().getName().getId() + "_taint.dot")) {
            for (Tree<BasicBlock> tree : trees)
                tree.toDot(dot);
        } catch (IOException e) {
            err("I/O error", e);
        }

        cfg.removeSSA();

        info("End of the taint analysis");
        info("Trees saved into output/" + cfg.getMain().getName().getId() + "_taint.dot");

        return trees;
    }
}
