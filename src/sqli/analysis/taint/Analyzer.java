package sqli.analysis.taint;

import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.CFG;
import sqli.analysis.ssa.PhiFunction;
import sqli.lang.absyn.*;
import sqli.lang.absyn.ExtendedUse.ExtUse;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Analyzer {

    private final CFG cfg;

    Analyzer(CFG cfg) {
        this.cfg = cfg;
    }

    Set<Tree<BasicBlock>> run() {
        Set<Tree<BasicBlock>> trees = new HashSet<>();
        for (SubProgram prog : cfg.getProgs())
            trees.addAll(analyzeExecutes(prog));

        return trees;
    }

    private Set<Tree<BasicBlock>> analyzeExecutes(SubProgram prog) {
        Set<Tree<BasicBlock>> trees = new HashSet<>();
        //noinspection Convert2streamapi
        for (BasicBlock block : cfg.getBlocksOf(prog))
            if (block.getAbsyn() instanceof Execute)
                trees.add(taint(block, new CallHistory(), prog));

        return trees;
    }

    private Tree<BasicBlock> taint(BasicBlock b, CallHistory c, SubProgram prog) {
        Analyzable com = b.getAbsyn();

        if (com.extendedUse().isEmpty() && com instanceof VariableDeclaration) {
            Set<Tree<?>> child = new HashSet<>();
            child.add(taint(new Bottom(((VariableDeclaration) com).getParam()), new CallHistory(c)));

            return new Tree<>(b, child);
        }

        if (com.extendedUse().isEmpty())
            return new Tree<>(b, new HashSet<>());

        Set<Tree<?>> children = new HashSet<>();
        List<ExtUse> extendedUse = new ArrayList<>(com.extendedUse());

        while (!extendedUse.isEmpty()) {
            ExtUse u = extendedUse.remove(0);

            if (u instanceof VariableName) {
                VariableName varName = (VariableName) u;
                BasicBlock nextBlock = cfg.reachingDefinitions(prog, varName.getSsaName());

                if (nextBlock.getPhiFunctions().get(varName.getId()) != null) {
                    PhiFunction phi = nextBlock.getPhiFunctions().get(varName.getId());
                    extendedUse.addAll(phi.extendedUse());
                } else {
                    children.add(taint(nextBlock, new CallHistory(c), prog));
                }
            } else {
                Call call = (Call) u;
                CallHistory newHistory = new CallHistory(c);
                newHistory.add(new BlockCall(b, call, prog));
                children.add(taint(call, newHistory));
            }
        }

        return new Tree<>(b, children);
    }

    private Tree<Call> taint(Call f, CallHistory c) {
        if (c.isLastDuplicate())
            return new Tree<>(f, new HashSet<>());

        FunctionDefinition def = null;
        for (FunctionDefinition funDef : cfg.getMain().collectFunctionDefinitions())
            if (funDef.getDecl().getFun().getId().equals(f.getFun().getId())) {
                def = funDef;
                break;
            }

        @SuppressWarnings("ConstantConditions") Set<Return> ret = def.ret();
        Set<Tree<?>> children = new HashSet<>();
        for (Return r : ret)
            children.add(taint(r.toCFGBlock(), new CallHistory(c), def));

        return new Tree<>(f, children);
    }

    private Tree<?> taint(Bottom bot, CallHistory c) {
        Expression actualParameter = c.last().getCall().getArgs().getArg(bot.getK() - 1);

        if (actualParameter.extendedUse().isEmpty())
            return new Tree<>(new Bottom(0), new HashSet<>());

        Set<ExtUse> extendedUse = actualParameter.extendedUse();
        Set<Tree<?>> children = new HashSet<>();

        BlockCall m = c.remove(c.size() - 1);
        for (ExtUse u : extendedUse) {

            if (u instanceof VariableName) {
                VariableName varName = (VariableName) u;
                children.add(taint(cfg.reachingDefinitions(
                        m.getProg(), varName.getSsaName()), new CallHistory(c), m.getProg()));
            } else {
                Call call = (Call) u;
                CallHistory newHistory = new CallHistory(c);
                newHistory.add(new BlockCall(m.getCaller(), call, m.getProg()));
                children.add(taint(call, newHistory));
            }
        }

        return new Tree<>(m.getCaller(), children);
    }
}
