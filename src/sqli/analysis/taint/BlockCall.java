package sqli.analysis.taint;

import sqli.analysis.cfg.BasicBlock;
import sqli.lang.absyn.Call;
import sqli.lang.absyn.SubProgram;

class BlockCall {

    private final BasicBlock caller;
    private final Call call;
    private final SubProgram prog;

    BlockCall(BasicBlock caller, Call call, SubProgram prog) {
        this.caller = caller;
        this.call = call;
        this.prog = prog;
    }

    BasicBlock getCaller() {
        return caller;
    }

    Call getCall() {
        return call;
    }

    SubProgram getProg() {
        return prog;
    }
}
