package controller;

import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sqli.analysis.cfg.BasicBlock;
import sqli.analysis.cfg.CFG;
import sqli.analysis.contracts.Path;
import sqli.analysis.taint.Tree;
import sqli.lang.absyn.ProgDefinition;
import view.cfg.CFGUI;
import view.contracts.AskForContractUI;
import view.contracts.ContractsUI;
import view.lexical.LexicalUI;
import view.monitor.MonitorUI;
import view.semantical.SemanticalUI;
import view.ssa.SSAUI;
import view.syntactical.SyntacticalUI;
import view.taint.TaintUI;

import java.util.*;

import static sqli.analysis.cfg.Main.runCfgConstruction;
import static sqli.analysis.contracts.Main.runContractsRequest;
import static sqli.analysis.monitor.Main.runMonitor;
import static sqli.analysis.ssa.Main.runSsaConstruction;
import static sqli.analysis.taint.Main.runTaintAnalysis;
import static sqli.lang.lexical.Lexer.mkLexer;
import static sqli.lang.lexical.Main.runLexicalAnalysis;
import static sqli.lang.semantical.Main.runSemanticalAnalysis;
import static sqli.lang.syntactical.Main.runSyntacticalAnalysis;
import static utils.Utils.checkFilename;

public class ProcessHandler {

    private static ProcessHandler instance;

    private final List<String> errs;
    private final List<String> info;
    private final Map<Integer, String> phases;

    private Integer phase;
    private String source;

    private Stage primaryStage;
    private Stage lexicalStage;
    private Stage syntacticalStage;
    private Stage semanticalStage;
    private Stage cfgStage;
    private Stage ssaStage;
    private Stage taintStage;
    private Stage contractsStage;
    private AskForContractUI askForContractStage;
    private Stage monitorStage;

    private ProgDefinition absyn;
    private CFG cfg;
    private Set<Tree<BasicBlock>> trees;
    private Set<Path> paths;

    private ProcessHandler() {
        errs = new ArrayList<>();
        info = new ArrayList<>();
        phases = new HashMap<Integer, String>() {{
            put(0, "Initialization");
            put(1, "Lexical analysis");
            put(2, "Syntactical analysis");
            put(3, "Semantical analysis");
            put(4, "Control Flow Graph construction");
            put(5, "SSA form construction");
            put(6, "Taint analysis");
            put(7, "Contracts request");
            put(8, "Monitoring");
        }};
    }

    public static ProcessHandler getInstance() {
        if (instance == null)
            instance = new ProcessHandler();

        return instance;
    }

    public String getSource() {
        return source;
    }

    public boolean init(String source) {
        phase = 0;

        if (source.contains("\\"))
            source = source.replace("\\", "/");

        checkFilename(source);
        this.source = source;

        return processMsgs();
    }

    public ProgDefinition getAbsyn() {
        return absyn;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void setLexicalStage(Stage lexicalStage) {
        this.lexicalStage = lexicalStage;
    }

    public void setSyntacticalStage(Stage syntacticalStage) {
        this.syntacticalStage = syntacticalStage;
    }

    public void setSemanticalStage(Stage semanticalStage) {
        this.semanticalStage = semanticalStage;
    }

    public void setCFGStage(Stage cfgStage) {
        this.cfgStage = cfgStage;
    }

    public void setSSAStage(Stage ssaStage) {
        this.ssaStage = ssaStage;
    }

    public void setTaintStage(Stage taintStage) {
        this.taintStage = taintStage;
    }

    public void setContractsStage(Stage contractsStage) {
        this.contractsStage = contractsStage;
    }

    public AskForContractUI getAskForContractStage() {
        return askForContractStage;
    }

    public void setAskForContractStage(AskForContractUI askForContractStage) {
        this.askForContractStage = askForContractStage;
    }

    public void setMonitorStage(Stage monitorStage) {
        this.monitorStage = monitorStage;
    }

    public void spawnLexicalStage() {
        LexicalUI lexicalUI = new LexicalUI();
        lexicalUI.initModality(Modality.APPLICATION_MODAL);
        lexicalUI.show();
    }

    public void closeLexicalStage() {
        lexicalStage.close();
    }

    public void spawnSyntacticalStage() {
        closeLexicalStage();

        SyntacticalUI syntacticalUI = new SyntacticalUI();
        syntacticalUI.initModality(Modality.APPLICATION_MODAL);
        syntacticalUI.show();
    }

    public void closeSyntacticalStage() {
        syntacticalStage.close();
    }

    public void spawnSemanticalStage() {
        closeSyntacticalStage();

        SemanticalUI semanticalUI = new SemanticalUI();
        semanticalUI.initModality(Modality.APPLICATION_MODAL);
        semanticalUI.show();
    }

    public void closeSemanticalStage() {
        semanticalStage.close();
    }

    public void spawnCFGStage() {
        closeSemanticalStage();

        CFGUI cfgUI = new CFGUI();
        cfgUI.initModality(Modality.APPLICATION_MODAL);
        cfgUI.show();
    }

    public void closeCFGStage() {
        cfgStage.close();
    }

    public void spawnSSAStage() {
        closeCFGStage();

        SSAUI ssaUI = new SSAUI();
        ssaUI.initModality(Modality.APPLICATION_MODAL);
        ssaUI.show();
    }

    public void closeSSAStage() {
        ssaStage.close();
    }

    public void spawnTaintStage() {
        closeSSAStage();

        TaintUI taintUI = new TaintUI();
        taintUI.initModality(Modality.APPLICATION_MODAL);
        taintUI.show();
    }

    public void closeTaintStage() {
        taintStage.close();
    }

    public void spawnContractsStage() {
        closeTaintStage();

        ContractsUI contractsUI = new ContractsUI();
        contractsUI.initModality(Modality.APPLICATION_MODAL);
        contractsUI.show();
    }

    public void closeContractsStage() {
        contractsStage.close();
    }

    public void spawnAskForContractStage(Path path) {
        AskForContractUI askForContractUI = new AskForContractUI(path);
        askForContractUI.initModality(Modality.APPLICATION_MODAL);
        askForContractUI.showAndWait();
    }

    public void closeAskForContractStage() {
        askForContractStage.close();
    }

    public void spawnMonitorStage() {
        closeContractsStage();

        MonitorUI monitorUI = new MonitorUI();
        monitorUI.initModality(Modality.APPLICATION_MODAL);
        monitorUI.show();
    }

    public void closeMonitorStage() {
        monitorStage.close();
    }

    public boolean startLexicalAnalysis() {
        phase = 1;
        runLexicalAnalysis(source, mkLexer(source));
        return processMsgs();
    }

    public boolean startSyntacticalAnalysis() {
        phase = 2;
        absyn = runSyntacticalAnalysis(mkLexer(source));
        return processMsgs();
    }

    public boolean startSemanticalAnalysis() {
        phase = 3;
        runSemanticalAnalysis(absyn);
        return processMsgs();
    }

    public boolean startCFGConstruction() {
        phase = 4;
        cfg = runCfgConstruction(absyn);
        return processMsgs();
    }

    public boolean startSSAConstruction() {
        phase = 5;
        runSsaConstruction(cfg);
        return processMsgs();
    }

    public boolean startTaintAnalysis() {
        phase = 6;
        trees = runTaintAnalysis(cfg);
        return processMsgs();
    }

    public boolean startContractsRequest() {
        phase = 7;
        paths = runContractsRequest(trees, cfg.getMain().getName().getId());
        return processMsgs();
    }

    public boolean startMonitoring() {
        phase = 8;
        runMonitor(cfg, paths);
        return processMsgs();
    }

    public void end() {
        primaryStage.close();
    }

    public void asyncErr(String msg, String title) {
        alert(msg, title, null, Alert.AlertType.ERROR);
    }

    void addErrMsg(String err) {
        errs.add(err);
    }

    void addInfoMsg(String info) {
        this.info.add(info);
    }

    private boolean processMsgs() {
        if (!errs.isEmpty()) {
            errAlert();
            errs.clear();
            return false;
        }

        if (!info.isEmpty()) {
            infoAlert();
            info.clear();
        }

        return true;
    }

    private void errAlert() {
        String msg = "";
        for (String s : errs)
            msg += s + "\n";

        alert(msg.substring(0, msg.length() - 1), "Error", phases.get(phase), Alert.AlertType.ERROR);
    }

    private void infoAlert() {
        String msg = "";
        for (String s : info)
            msg += s + "\n";

        alert(msg.substring(0, msg.length() - 1), "Information", phases.get(phase), Alert.AlertType.INFORMATION);
    }

    public void alert(String msg, String title, String headerText, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(msg);
        alert.setResizable(true);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);

        alert.showAndWait();
    }

    private String input(String type) {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle(type + " input");
        dialog.setHeaderText("Input");
        dialog.setContentText("Please insert a " + type + " input:");

        Optional<String> result = dialog.showAndWait();
        if (!result.isPresent()) {
            alert("Input error", "Invalid input", "Please insert a valid input", Alert.AlertType.ERROR);
            return input(type);
        }

        return result.get();
    }

    public Integer inputInteger() {
        String in = input("Integer");
        try {
            return Integer.parseInt(in);
        } catch (NumberFormatException e) {
            alert("Input error", "Invalid input", "Please insert a valid input", Alert.AlertType.ERROR);
            return inputInteger();
        }
    }

    public Float inputFloat() {
        String in = input("Float");
        try {
            return Float.parseFloat(in);
        } catch (NumberFormatException e) {
            alert("Input error", "Invalid input", "Please insert a valid input", Alert.AlertType.ERROR);
            return inputFloat();
        }
    }

    public Boolean inputBoolean() {
        String in = input("Boolean");
        try {
            return Boolean.parseBoolean(in);
        } catch (NumberFormatException e) {
            alert("Input error", "Invalid input", "Please insert a valid input", Alert.AlertType.ERROR);
            return inputBoolean();
        }
    }

    public String inputString() {
        return input("String");
    }
}
