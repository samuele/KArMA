package controller;

public class ErrorsHandler {

    private static ProcessHandler pHandler;

    public static void init(ProcessHandler pHandler) {
        ErrorsHandler.pHandler = pHandler;
    }

    public static void err(String err) {
        pHandler.addErrMsg(err);
    }

    public static void err(String err, Exception e) {
        err(err + ": " + e.getMessage());
    }

    public static void info(String info) {
        pHandler.addInfoMsg(info);
    }
}
