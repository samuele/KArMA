package dot;

import java.io.FileWriter;
import java.io.IOException;

public class Dot implements AutoCloseable {

    private final String filename;
    private final FileWriter where;

    private int counter;

    public Dot(String filename) throws IOException {
        this.filename = filename;

        where = new FileWriter(filename);
        where.write("digraph {\n");
        where.write("size = \"11,7.5\";\n");

        counter = 0;
    }

    public String getFilename() {
        return filename;
    }

    private String dotSymbol(String symbol, String shape) throws IOException {
        String id = "symbol_" + (counter++);
        where.write(id + " [label = \"" + symbol + "\" fontname = \"Times-Italic\" shape = " + shape + "]\n");

        return id;
    }

    public String dotSymbol(String symbol) throws IOException {
        return dotSymbol(symbol, "box");
    }

    public String dotDoubleCircleSymbol(String symbol) throws IOException {
        return dotSymbol(symbol, "doublecircle");
    }

    private DotableNode dotNode(DotableNode node, String shape, String color) throws IOException {
        where.write(node.setNodeName("node_" + (counter++)) + " [label = \""
                + (node.label().replace("\\", "\\\\")).replace("\"", "\\\"") + "\" shape = " + shape + " color = " + color + "];\n");

        return node;
    }

    public DotableNode dotNode(DotableNode node) throws IOException {
        return dotNode(node, "oval", "black");
    }

    public DotableNode dotRedBoxNode(DotableNode node) throws IOException {
        return dotNode(node, "box", "red");
    }

    public DotableNode dotBoxNode(DotableNode node) throws IOException {
        return dotNode(node, "box", "black");
    }

    public DotableNode dotHexagonNode(DotableNode node) throws IOException {
        return dotNode(node, "hexagon", "black");
    }

    private DotableNode linkNodeToNode(DotableNode from, DotableNode to, String label, String style, String color)
            throws IOException {
        where.write(from.getNodeName() + " -> " + to.getNodeName() + " [label = \"" + label + "\" fontsize = 8 style = "
                + style + " color = " + color + "]\n");

        return from;
    }

    public DotableNode linkNodeToNode(DotableNode from, DotableNode to, String label) throws IOException {
        return linkNodeToNode(from, to, label, "solid", "black");
    }

    public DotableNode redLinkNodeToNode(DotableNode from, DotableNode to, String label) throws IOException {
        return linkNodeToNode(from, to, label, "solid", "red");
    }

    public DotableNode redDashedLinkNodeToNode(DotableNode from, DotableNode to, String label) throws IOException {
        return linkNodeToNode(from, to, label, "dashed", "red");
    }

    public DotableNode boldLinkNodeToNode(DotableNode from, DotableNode to, String label) throws IOException {
        return linkNodeToNode(from, to, label, "bold", "black");
    }

    private DotableNode linkNodeToSymbol(DotableNode from, String to, String label, String style, String color)
            throws IOException {
        where.write(from.getNodeName() + " -> " + to + " [label = \"" + label + "\" fontsize = 8 style = " + style
                + " color = " + color + "]\n");

        return from;
    }

    public DotableNode linkNodeToSymbol(DotableNode from, String to, String label) throws IOException {
        return linkNodeToSymbol(from, to, label, "solid", "black");
    }

    private String linkSymbolToNode(String from, DotableNode to, String label, String style, String color)
            throws IOException {
        where.write(from + " -> " + to.getNodeName() + " [label = \"" + label + "\" fontsize = 8 style = " + style
                + " color = " + color + "]\n");

        return from;
    }

    public String linkSymbolToNode(String from, DotableNode to, String label) throws IOException {
        return linkSymbolToNode(from, to, label, "solid", "black");
    }

    public void close() throws IOException {
        where.write("}");
        where.close();
    }
}
