package dot;

import java.io.IOException;

public interface DotableNode extends Dotable {

    String getNodeName();

    String setNodeName(String nodeName);

    String label();

    @Override
    DotableNode toDot(Dot dot) throws IOException;
}
