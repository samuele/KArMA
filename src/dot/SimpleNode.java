package dot;

import java.io.IOException;

public class SimpleNode implements DotableNode {

    private String nodeName;

    @Override
    public String getNodeName() {
        return nodeName;
    }

    @Override
    public String setNodeName(String nodeName) {
        return this.nodeName = nodeName;
    }

    @Override
    public String label() {
        return "";
    }

    @Override
    public DotableNode toDot(Dot dot) throws IOException {
        return dot.dotBoxNode(this);
    }
}
