package dot;

import java.io.IOException;

public interface Dotable {

    @SuppressWarnings("unused")
    Dotable toDot(Dot dot) throws IOException;
}
